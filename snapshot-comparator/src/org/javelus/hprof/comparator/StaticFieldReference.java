package org.javelus.hprof.comparator;

import com.sun.tools.hat.internal.model.JavaClass;
import com.sun.tools.hat.internal.model.JavaField;
import com.sun.tools.hat.internal.model.Root;


public class StaticFieldReference extends RootNode {

    private JavaClass klass;
    private JavaField field;

    private String identity;

    public StaticFieldReference(Root root, JavaClass klass, JavaField field) {
        super(root);
        this.klass = klass;
        this.field = field;
        this.identity = String.format("%s.%s %s", klass.getName(), field.getName(), field.getSignature());
    }

    public String getIdentity() {
        return identity;
    }

    public JavaClass getJavaClass() {
        return klass;
    }

    public JavaField getJavaField() {
        return field;
    }
}

package org.javelus.hprof.comparator;

import com.sun.tools.hat.internal.model.Root;

public abstract class RootNode {
    protected Root root;

    public RootNode(Root root) {
        this.root = root;
    }

    public Root getRoot() {
        return root;
    }

    public abstract String getIdentity();

}

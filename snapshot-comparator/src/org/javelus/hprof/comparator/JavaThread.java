package org.javelus.hprof.comparator;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.javelus.minitracing.ThreadTrace;

import com.sun.tools.hat.internal.model.JavaHeapObject;
import com.sun.tools.hat.internal.model.JavaObject;
import com.sun.tools.hat.internal.model.StackFrame;
import com.sun.tools.hat.internal.model.StackTrace;

public class JavaThread {
    private JavaObject threadObject;

    /**
     * The top most is at the beginning.
     */
    private List<LocalReference> locals;

    private Set<JavaHeapObject> reachableObjects;

    private ThreadTrace threadTrace;
    
    // ordered by stack frame and slot
    private Frame[] frames;

    public JavaThread(JavaObject threadObject) {
        this.threadObject = threadObject;
        locals = new ArrayList<LocalReference>();
    }

    void buildFrames() {
        StackTrace stackTrace = getStackTrace();
        StackFrame[] stackFrames = stackTrace.getFrames();
        int height = stackFrames.length;
        frames = new Frame[height];
        // in reverser order, last slot is first
        for (int i=locals.size()-1; i>=0; i--) {
            LocalReference lr = locals.get(i);
            int index = lr.getDepth() - 1;
            Frame frame = frames[index];
            if (frame == null) {
                frame = new Frame(lr.slot + 1, stackFrames[index]);
                frames[index] = frame;
            }
            frame.setLocal(lr.slot, lr);
        }

        for (int i=0; i<frames.length; i++) {
            if (frames[i] == null) {
                frames[i] = new Frame(0, stackFrames[i]);
            }
        }
    }

    /**
     * locals are added from the top most frame to the bottom most one.
     * @param local
     */
    public void addLocalReference(LocalReference local) {
        this.locals.add(local);
    }

    public JavaObject getJavaObject() {
        return threadObject;
    }

    public int getHeight() {
        StackTrace stackTrace = getStackTrace();
        if (stackTrace == null) {
            return -1;
        }
        return stackTrace.getFrames().length;
    }

    public StackTrace getStackTrace() {
        if (this.locals.isEmpty()) {
            return null;
        }
        return this.locals.get(this.locals.size() - 1).getRoot().getStackTrace();
    }

    public void setReachableObjects(Set<JavaHeapObject> reachableObjects) {
        this.reachableObjects = reachableObjects;
    }

    public Set<JavaHeapObject> getReachableObjects() {
        return reachableObjects;
    }

    public List<LocalReference> getLocals() {
        return locals;
    }
    
    public Frame[] getFrames() {
        return frames;
    }
    
    public String getIdentity() {
        return getJavaObject().getField("tid").toString();
    }

    public ThreadTrace getThreadTrace() {
        return threadTrace;
    }

    public void setThreadTrace(ThreadTrace threadTrace) {
        this.threadTrace = threadTrace;
    }
}

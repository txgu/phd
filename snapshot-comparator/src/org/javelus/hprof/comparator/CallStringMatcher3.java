package org.javelus.hprof.comparator;

import java.util.LinkedList;
import java.util.Map.Entry;

import org.javelus.minitracing.AllocationEntry;
import org.javelus.minitracing.ThreadTraces;

import com.sun.tools.hat.internal.model.JavaClass;
import com.sun.tools.hat.internal.model.JavaHeapObject;
import com.sun.tools.hat.internal.model.JavaObject;
import com.sun.tools.hat.internal.model.JavaObjectArray;
import com.sun.tools.hat.internal.model.JavaThing;

public class CallStringMatcher3 extends Matcher {

    public CallStringMatcher3(ObjectGraph oldHeap, ObjectGraph newHeap, ThreadTraces oldTrace, ThreadTraces newTrace, MethodMetaComparator comparator) {
        super(oldHeap, newHeap, oldTrace, newTrace, comparator);
    }

    protected void matchAndVisitJavaObjectArray(LinkedList<JavaHeapObject> queue,
            JavaObjectArray oldJOA, JavaObjectArray newJOA, JavaClass oldClass,
            JavaClass newClass) {
        final int oldLength = oldJOA.getLength();
        final int newLength = newJOA.getLength();

        JavaThing[] oldElements = oldJOA.getElements();
        JavaThing[] newElements = newJOA.getElements();
        for (int i=0; i<oldLength; i++) {
            JavaHeapObject oldE = (JavaHeapObject) oldElements[i];
            if (oldE == null) {
                continue;
            }
            long oldID = oldE.getId();
            AllocationEntry oldAE = oldTrace.getAllocationEntryFromToPointer(oldID, oldHeap.getTimestamp());
            if (oldAE == null) {
                if (i < newLength) {
                    JavaHeapObject newE = (JavaHeapObject) newElements[i];
                    if (newE != null 
                            && newTrace.getAllocationEntryFromToPointer(newE.getId(), newHeap.getTimestamp()) == null) {
                        // two objects have no id
                        compareAndAppendIntoQueue(queue, oldE, newE);
                    }
                }
                continue;
            }

            JavaHeapObject newE = findMatchedNewObjectByAllocationEntry(oldE, oldAE);
            if (newE == null) {
                if (i < newLength) {
                    newE = (JavaHeapObject) newElements[i];
                    if (newE == null) { // cannot match objects by access path 
                        continue;
                    }
                    AllocationEntry newAE = newTrace.getAllocationEntryFromToPointer(newE.getId(), newHeap.getTimestamp());
                    if (newAE == null) {
                        continue;
                    }
                    JavaHeapObject checkE = findMatchedOldObjectByAllocationEntry(newE, newAE);
                    if (checkE == null) {
                        // both oldE and newE has no matched objects via EI
                        // match them via access path
                        compareAndAppendIntoQueue(queue, oldE, newE);
                    }
                }
                continue;
            }

            compareAndAppendIntoQueue(queue, oldE, newE);
        }
    }

    protected void matchAndVisitJavaObject(LinkedList<JavaHeapObject> queue,
            JavaObject oldJO, JavaObject newJO, JavaClass oldClass,
            JavaClass newClass) {
        Matching<InstanceField> fieldsMatching = getMatchedInstanceFields(oldClass, newClass);
        JavaThing[] oldFieldValues = oldJO.getFields();
        JavaThing[] newFieldValues = newJO.getFields();

        for (Entry<InstanceField, InstanceField> entry:fieldsMatching.getMatched().entrySet()) {
            InstanceField oldField = entry.getKey();
            InstanceField newField = entry.getValue();
            JavaThing oldFieldValue = oldFieldValues[oldField.getIndex()];
            JavaThing newFieldValue = newFieldValues[newField.getIndex()];
            if (oldFieldValue == oldNull && newFieldValue == newNull) {
                continue;
            }
            if (oldFieldValue == oldNull || newFieldValue == newNull) {
                continue;
            }
            if (oldFieldValue instanceof JavaHeapObject) {
                JavaHeapObject oldJHO = (JavaHeapObject) oldFieldValue;
                JavaHeapObject newJHO = (JavaHeapObject) newFieldValue;

                if (oldJHO == null || newJHO == null) {
                    continue;
                }

                AllocationEntry oldAE = oldTrace.getAllocationEntryFromToPointer(oldJHO.getId(), oldHeap.getTimestamp());
                AllocationEntry newAE = newTrace.getAllocationEntryFromToPointer(newJHO.getId(), newHeap.getTimestamp());

                if (oldAE == null && newAE == null) {
                    compareAndAppendIntoQueue(queue, oldJHO, newJHO);
                    continue;
                }

                if (oldAE != null && newAE != null && compareAllocationEntry(oldAE, newAE)) {
                    compareAndAppendIntoQueue(queue, oldJHO, newJHO);
                    continue;
                }

                if (oldAE != null) {
                    JavaHeapObject checkJHO = findMatchedNewObjectByAllocationEntry(oldJHO, oldAE);
                    if (checkJHO == newJHO) {
                        throw new RuntimeException("Sanity check failed!");
                    }
                    if (checkJHO != null) {
                        continue;
                    }
                }

                if (newAE != null) {
                    JavaHeapObject checkJHO = findMatchedOldObjectByAllocationEntry(newJHO, newAE);
                    if (checkJHO == oldJHO) {
                        throw new RuntimeException("Sanity check failed!");
                    }
                    if (checkJHO != null) {
                        continue;
                    }
                }

                compareAndAppendIntoQueue(queue, oldJHO, newJHO);
            }
        }
    }
}

package org.javelus.hprof.comparator;

import com.sun.tools.hat.internal.model.Root;

public class LocalReference extends RootNode{

    final JavaThread thread;
    final int slot;
    private int height;
    private String identity;
    public LocalReference(JavaThread thread, Root root, int slot) {
        super(root);
        this.thread = thread;
        this.slot = slot;
        this.height = -1;
        this.identity = String.format("%s@%d@%d", thread.getIdentity(), getDepth(), slot);
    }

    public int getDepth() {
        return root.getStackTrace().getFrames().length;
    }
    
    public int getHeight() {
        return height;
    }
    
    void setHeight(int height) {
        this.height = height;
    }

    @Override
    public String getIdentity() {
        return identity;
    }
}

package org.javelus.hprof.comparator;

import org.javelus.minitracing.MethodMeta;

public interface MethodMetaComparator {
    /**
     * return true if equal
     * @param oldMethod
     * @param newMethod
     * @return
     */
    boolean compareMethodMeta(MethodMeta oldMethod, MethodMeta newMethod);
}

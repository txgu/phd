package org.javelus.hprof.comparator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.javelus.minitracing.ThreadTrace;

import com.sun.tools.hat.internal.model.JavaClass;
import com.sun.tools.hat.internal.model.JavaField;
import com.sun.tools.hat.internal.model.JavaHeapObject;
import com.sun.tools.hat.internal.model.JavaObject;
import com.sun.tools.hat.internal.model.JavaStatic;
import com.sun.tools.hat.internal.model.JavaThing;
import com.sun.tools.hat.internal.model.Root;
import com.sun.tools.hat.internal.model.Snapshot;
import com.sun.tools.hat.internal.util.ArraySorter;
import com.sun.tools.hat.internal.util.Comparer;

public class ObjectGraph {

    private Map<String, StaticFieldReference> staticFields = new HashMap<String, StaticFieldReference>();

    private Map<String, JavaClass> classes = new HashMap<String, JavaClass>();

    private List<JavaThread> javaThreads;

    private Snapshot snapshot;

    private Set<JavaHeapObject> reachableFromStatics;

    private Map<JavaClass, InstanceField[]> classToInstanceFields = new HashMap<JavaClass, InstanceField[]>();

    private long timestamp;
    
    public ObjectGraph(Snapshot snapshot, long timestamp) {
        this.snapshot = snapshot;
        this.timestamp = timestamp;
        buildClasses();
        buildRoot();
    }
    
    public long getTimestamp() {
        return timestamp;
    }
    
    public JavaThread getThreadByTrace(ThreadTrace trace) {
        JavaThread existing = (JavaThread) trace.getJavaObject();
        if (existing != null) {
            return existing;
        }
        long eetop = trace.getThreadID();
        String eetopString = String.valueOf(eetop);
        for (JavaThread thread : getThreads()) {
            if (eetopString.equals(thread.getJavaObject().getField("eetop").toString())) {
                trace.setJavaObject(thread);
                thread.setThreadTrace(trace);
                return thread;
            }
        }
        throw new RuntimeException();
    }

    public InstanceField[] getInstanceFields(JavaClass clazz) {
        InstanceField[] instanceFields = classToInstanceFields.get(clazz);
        if (instanceFields == null) {
            JavaField[] fields = clazz.getFieldsForInstance();
            InstanceField[] superInstanceFields = null;
            if (clazz.getSuperclass() != null) {
                superInstanceFields = getInstanceFields(clazz.getSuperclass());
            } else {
                superInstanceFields = new InstanceField[0];
            }
            instanceFields = new InstanceField[fields.length];
            int length = superInstanceFields.length;
            System.arraycopy(superInstanceFields, 0, instanceFields, 0, length);
            for (int i = length; i < fields.length; i++) {
                instanceFields[i] = new InstanceField(clazz, fields[i], i);
            }
            classToInstanceFields.put(clazz, instanceFields);
        }
        return instanceFields;
    }
    
    
    public Iterator<JavaClass> getClasses() {
        return this.classes.values().iterator();
    }
    
    public JavaClass getClazz(String name) {
        return classes.get(name);
    }

    @SuppressWarnings("unchecked")
    private void buildClasses() {
        Iterator<JavaClass> it = (Iterator<JavaClass>)this.snapshot.getClasses();
        while (it.hasNext()) {
            JavaClass clazz = it.next();
            if (classes.put(clazz.getName(), clazz) != null) {
                throw new RuntimeException("Duplicated classes!");
            }
        }
    }

    /**
     * @see com.sun.tools.hat.internal.model.JavaStatic
     */
    private static final int PREFIX_LENGTH = "Static reference from ".length();

    private void buildRoot() {
        Root[] roots = snapshot.getRootsArray();
        ArraySorter.sort(roots, new Comparer() {
            public int compare(Object lhs, Object rhs) {
                Root left = (Root) lhs;
                Root right = (Root) rhs;
                return left.getIndex() - right.getIndex(); // !!! index is used to identify local in the same frame
            }
        });

        int lastSlot = 0;
        int lastDepth = 0;
        JavaHeapObject lastReferer = null;
        JavaThread lastJavaThread = null;
        javaThreads = new ArrayList<JavaThread>();
        staticFields = new HashMap<String, StaticFieldReference>();
        for (int i= 0; i < roots.length; i++) {
            Root root = roots[i];
            if (root.getType() == Root.JAVA_LOCAL) {
                if (lastReferer != root.getReferer()) {
                    lastReferer = root.getReferer();
                    lastJavaThread = new JavaThread((JavaObject)lastReferer);
                    javaThreads.add(lastJavaThread);
                    lastDepth = root.getStackTrace().getFrames().length;
                    lastSlot = 0;
                } else if (lastDepth !=  root.getStackTrace().getFrames().length) {
                    lastDepth =  root.getStackTrace().getFrames().length;
                    lastSlot = 0;
                } else {
                    lastSlot ++;
                }

                LocalReference lr = new LocalReference(lastJavaThread, root, lastSlot);
                lastJavaThread.addLocalReference(lr);
                continue;
            }

            if (root.getType() == Root.JAVA_STATIC) {
                String description = root.getDescription();

                int lastDot = description.lastIndexOf('.');

                if (lastDot == -1) {
                    throw new RuntimeException("Oops");
                }

                String className = description.substring(PREFIX_LENGTH, lastDot);
                String fieldName = description.substring(lastDot + 1);

                JavaClass klass = snapshot.findClass(className);
                if (klass == null) {
                    throw new RuntimeException("Cannot find class " + className);
                }

                JavaField field = null;
                for (JavaStatic s:klass.getStatics()) {
                    JavaField sf = s.getField();
                    if (sf.getName().equals(fieldName)) {
                        field = sf;
                        break;
                    }
                }

                if (field == null) {
                    throw new RuntimeException("Cannot find field " + fieldName + " in class " + className);
                }

                StaticFieldReference sfr = new StaticFieldReference(root, klass, field);
                staticFields.put(sfr.getIdentity(), sfr);
                continue;
            }
        }
        
        for (JavaThread jt:javaThreads) {
            ReachableObjectsVisitor visitor = new ReachableObjectsVisitor();
            for (LocalReference lr:jt.getLocals()) {
                visitor.visit(getPointedTo(lr));
            }
            jt.setReachableObjects(visitor.getReachableObjects());
            
            int height = jt.getHeight();
            for (LocalReference lr:jt.getLocals()) {
                Root r = lr.getRoot();
                int depth = r.getStackTrace().getFrames().length;
                lr.setHeight(height-depth);
            }
            
            jt.buildFrames();
        }
        
        ReachableObjectsVisitor visitor = new ReachableObjectsVisitor();
        for (StaticFieldReference sfr:staticFields.values()) {
            JavaHeapObject jho = getPointedTo(sfr);
            visitor.visit(jho);
        }
        reachableFromStatics = visitor.getReachableObjects();
    }

    public Map<String, StaticFieldReference> getStaticFields() {
        return staticFields;
    }

    public StaticFieldReference getStaticField(String id) {
        return staticFields.get(id);
    }

    public JavaHeapObject getPointedTo(RootNode root) {
        return snapshot.findThing(root.getRoot().getId());
    }

    public JavaThing getFieldValue(JavaObject object, JavaField field) {
        JavaThing[] flds = object.getFields();
        JavaField[] instFields = object.getClazz().getFieldsForInstance();
        String name = field.getName();
        String signature = field.getSignature();
        for (int i = 0; i < instFields.length; i++) {
            if (instFields[i].getName().equals(name)
                    && instFields[i].getSignature().equals(signature)) {
                return flds[i];
            }
        }
        throw new RuntimeException("Should not reach here!");
    }

    public Set<JavaHeapObject> getReachableFromStatics() {
        return reachableFromStatics;
    }

    public List<JavaThread> getThreads() {
        return javaThreads;
    }
    
    public Snapshot getSnapshot() {
        return snapshot;
    }

    public String getObjectIdentity(JavaObject object) {
        return object.getIdString();
    }
}

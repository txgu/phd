package org.javelus.hprof.comparator;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.sun.tools.hat.internal.model.JavaHeapObject;

public class Matching<E> {

    private Set<E> newElements;
    private Set<E> oldElements;
    private Map<E, E> matchedElements;
    private Map<E, E> matchedElementsReverse;

    private Set<E> changedElements;

    public Matching() {
        newElements = new HashSet<E>();
        oldElements = new HashSet<E>();
        matchedElements = new HashMap<E, E>();
        matchedElementsReverse = new HashMap<E, E>();
        changedElements = new HashSet<E>();
    }

    public void addOld(E e) {
        if (isMatchedOld(e)) {
            throw new RuntimeException("Oops..");
        }
        this.oldElements.add(e);
    }

    public void addNew(E e) {
        if (isMatchedNew(e)) {
            throw new RuntimeException("Oops..");
        }
        this.newElements.add(e);
    }

    public boolean addMatched(E o, E n) {
        if (o == null || n == null) {
            throw new RuntimeException("Oops.");
        }
        if (isOld(o) || isNew(n)) {
            throw new RuntimeException("Oops.");
        }
        E nn = this.matchedElements.get(o);
        if (nn != null && nn != n) {
            return false;
        }

        E oo = this.matchedElementsReverse.get(n);
        if (oo != null && oo != o) {
            return false;
        }
        this.matchedElements.put(o, n);
        this.matchedElementsReverse.put(n, o);
        return true;
    }

    public boolean isNew(E n) {
        return newElements.contains(n);
    }

    public boolean isOld(E o) {
        return oldElements.contains(o);
    }

    public boolean isMatchedOld(E o) {
        return this.matchedElements.containsKey(o);
    }

    public boolean isMatchedNew(E n) {
        return this.matchedElementsReverse.containsKey(n);
    }

    public boolean isMatched(E o, E n) {
        return this.matchedElements.get(o) == n;
    }

    public void markChanged(E o, E n) {
        if (!isMatched(o, n)) {
            throw new RuntimeException("Changed objects should be matched first!");
        }
        changedElements.add(o);
        changedElements.add(n);
    }

    public Set<E> getNew() {
        return newElements;
    }

    public Set<E> getOld() {
        return oldElements;
    }

    public Map<E, E> getMatched() {
        return matchedElements;
    }

    public Set<E> getChanged() {
        return changedElements;
    }

    public Map<E, E> getMatchedReverse() {
        return matchedElementsReverse;
    }

    public String toString() {
        return String.format("old=%d, new=%d, matched=%d, changed=%d", 
                oldElements.size(),
                newElements.size(),
                matchedElements.size(),
                changedElements.size());
    }

    public double getMetric() {
        int removed = getOld().size();
        int added = getNew().size();
        int matched = getMatched().size();
        int changed = getChanged().size();

        int total = removed + added + matched * 2;

        return (removed + added + changed) / (double) total;
    }

    public boolean isChanged(JavaHeapObject o) {
        return changedElements.contains(o);
    }
}

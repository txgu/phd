package org.javelus.hprof.comparator;

import com.sun.tools.hat.internal.model.JavaClass;
import com.sun.tools.hat.internal.model.JavaField;

public class InstanceField {
    private JavaClass holder;
    private JavaField field;
    private int index;

    public InstanceField(JavaClass holder, JavaField field, int index) {
        this.holder = holder;
        this.field = field;
        this.index = index;
    }

    public JavaClass getHolder() {
        return holder;
    }
    
    public JavaField getField() {
        return field;
    }

    public int getIndex() {
        return index;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((field == null) ? 0 : field.hashCode());
        result = prime * result + ((holder == null) ? 0 : holder.hashCode());
        result = prime * result + index;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        InstanceField other = (InstanceField) obj;
        if (field == null) {
            if (other.field != null)
                return false;
        } else if (!field.equals(other.field))
            return false;
        if (holder == null) {
            if (other.holder != null)
                return false;
        } else if (!holder.equals(other.holder))
            return false;
        if (index != other.index)
            return false;
        return true;
    }
    
    public String getDeclaringClass() {
        return holder.getName();
    }

    public String getName() {
        return field.getName();
    }

    public String getSignature() {
        return field.getSignature();
    }
}

package org.javelus.hprof.comparator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.javelus.minitracing.AllocationEntry;
import org.javelus.minitracing.CallEntry;
import org.javelus.minitracing.ForwardPointers;
import org.javelus.minitracing.MethodMeta;
import org.javelus.minitracing.ThreadTrace;
import org.javelus.minitracing.ThreadTraces;

import com.sun.tools.hat.internal.model.JavaClass;
import com.sun.tools.hat.internal.model.JavaHeapObject;
import com.sun.tools.hat.internal.model.JavaObject;
import com.sun.tools.hat.internal.model.JavaObjectArray;
import com.sun.tools.hat.internal.model.JavaThing;
import com.sun.tools.hat.internal.model.JavaValue;
import com.sun.tools.hat.internal.model.JavaValueArray;
import com.sun.tools.hat.internal.model.StackFrame;

/**
 * Default matcher using a path in the BFS spanning tree as the id of an object
 * @author tianxiao
 *
 */
public class Matcher {

    private static final boolean printObjects = false;
    private static boolean onlyWithEI = true;

    private static final boolean printObjectsWithoutEI = false;
    private static final boolean printObjectsWithEI = false;
    private static final boolean onlyStatic = false;
    protected ObjectGraph oldHeap;
    protected ObjectGraph newHeap;
    protected ThreadTraces oldTrace;
    protected ThreadTraces newTrace;

    protected MethodMetaComparator methodComparator;
    
    protected Matching<StaticFieldReference> staticFieldsMatching;
    protected Matching<LocalReference> localsMatching;
    protected Matching<JavaHeapObject> objectsMatching;
    //protected Matching<JavaHeapObject> objectsMatchingWithEI;
    protected Matching<JavaThread> threadsMatching;

    protected Matching<JavaClass> classesMatching;

    protected Map<JavaClass, Matching<InstanceField>> oldClassToMatchedFields;

    final protected JavaThing oldNull;
    final protected JavaThing newNull;



    Map<JavaClass, Set<JavaHeapObject>> oldObjectsWithoutEI = new HashMap<JavaClass, Set<JavaHeapObject>>();
    Map<JavaClass, Set<JavaHeapObject>> newObjectsWithoutEI = new HashMap<JavaClass, Set<JavaHeapObject>>();


    Map<JavaClass, Set<JavaHeapObject>> oldObjectsWithEI = new HashMap<JavaClass, Set<JavaHeapObject>>();
    Map<JavaClass, Set<JavaHeapObject>> newObjectsWithEI = new HashMap<JavaClass, Set<JavaHeapObject>>();

    /**
     * Default: follow object array and not follow threads.
     * @param oldHeap
     * @param newHeap
     */
    public Matcher(ObjectGraph oldHeap, ObjectGraph newHeap, ThreadTraces oldTrace, ThreadTraces newTrace, MethodMetaComparator comparator) {
        this.oldHeap = oldHeap;
        this.newHeap = newHeap;
        this.oldTrace = oldTrace;
        this.newTrace = newTrace;
        
        if (comparator == null) {
            comparator = new MethodMetaComparator(){

                @Override
                public boolean compareMethodMeta(MethodMeta oldMethod,
                        MethodMeta newMethod) {
                    return oldMethod.equals(newMethod);
                }
                
            };
        } else {
            this.methodComparator = comparator;
        }

        oldNull = oldHeap.getSnapshot().getNullThing();
        newNull = newHeap.getSnapshot().getNullThing();

        this.threadsMatching = new Matching<JavaThread>();
        this.staticFieldsMatching = new Matching<StaticFieldReference>();
        this.localsMatching = new Matching<LocalReference>();
        this.objectsMatching = new Matching<JavaHeapObject>();
        //this.objectsMatchingWithEI = new Matching<JavaHeapObject>();
        this.classesMatching = new Matching<JavaClass>();
        oldClassToMatchedFields = new HashMap<JavaClass, Matching<InstanceField>>();
    }

    public void match() {
        long begin = System.nanoTime();
        matchClasses();
        matchStatics();
        matchThreads();
        matchObjects();
        compareMatchedObjects();
        long duration = System.nanoTime() - begin;
        
        System.out.format("Matching time: %d ns %d ms\n", duration, TimeUnit.NANOSECONDS.toMillis(duration));
        printAll();
    }

    protected void compareMatchedObjects() {
        for (Entry<JavaHeapObject, JavaHeapObject> entry : objectsMatching.getMatched().entrySet()) {
            JavaHeapObject oldObject = entry.getKey();
            JavaHeapObject newObject = entry.getValue();
            if (oldObject == oldNull || newObject == newNull) {
                throw new RuntimeException("Oops..");
            }
            JavaClass oldClass = oldObject.getClazz();
            JavaClass newClass = newObject.getClazz();
            if (!classesMatching.isMatched(oldClass, newClass)) {
                throw new RuntimeException("Oops..");
            }

            if (oldObject instanceof JavaObject) {
                if (!compareJavaObjects((JavaObject)oldObject, (JavaObject)newObject, oldClass, newClass)) {
                    objectsMatching.markChanged(oldObject, newObject);
                }
            } else if (oldObject instanceof JavaValueArray) {
                JavaValueArray oldJVA = (JavaValueArray) oldObject;
                JavaValueArray newJVA = (JavaValueArray) newObject;
                if (!compareValueArray(oldJVA, newJVA)) {
                    objectsMatching.markChanged(oldJVA, newJVA);
                }
            } else if (oldObject instanceof JavaObjectArray) {
                JavaObjectArray oldJOA = (JavaObjectArray) oldObject;
                JavaObjectArray newJOA = (JavaObjectArray) newObject;
                if (!compareObjectArray(oldJOA, newJOA)) {
                    objectsMatching.markChanged(oldJOA, newJOA);
                }
            } else if (oldObject instanceof JavaClass) {
                JavaClass oldClazz = (JavaClass) oldObject;
                JavaClass newClazz = (JavaClass) newObject;
                if (!oldClazz.getName().equals(newClazz.getName())) {
                    //                    String message = "Oops: classes matched by id " + oldClazz.getId()
                    //                    + " are not the same class, left is " + oldClazz.getName()
                    //                    + " and right is " + newClazz.getName();
                    //                    //throw new RuntimeException(message);
                    //                    System.err.println(message);
                    objectsMatching.markChanged(oldClazz, newClazz);
                }
            } else {
                System.err.println("Unsupported class: " + oldClass.getName());
                System.err.println("Unsupported class: " + newClass.getName());
            }
        }
    }

    protected boolean compareObjectArray(JavaObjectArray oldJOA, JavaObjectArray newJOA) {
        int length = oldJOA.getLength();
        if (newJOA.getLength() != length) {
            return false;
        }
        JavaThing[] oldElements = oldJOA.getElements();
        JavaThing[] newElements = newJOA.getElements();
        for (int i = 0; i < length; i++) {
            JavaHeapObject oldE = (JavaHeapObject) oldElements[i];
            JavaHeapObject newE = (JavaHeapObject) newElements[i];
            if (oldE == null && newE == null) {
                continue;
            }
            if (oldE == oldNull && newE == newNull) {
                throw new RuntimeException("Oops...");
            }
            if (oldE == null || newE == null) {
                continue;
            }
            if (!objectsMatching.isMatched(oldE, newE)) {
                return false;
            }
        }
        return true;
    }

    protected boolean compareJavaObjects(JavaObject oldObject, JavaObject newObject, JavaClass oldClass, JavaClass newClass) {
        Matching<InstanceField> fieldsMatching = getMatchedInstanceFields(oldClass, newClass);
        JavaThing[] oldFieldValues = oldObject.getFields();
        JavaThing[] newFieldValues = newObject.getFields();
        boolean changed = !fieldsMatching.getOld().isEmpty() || !fieldsMatching.getNew().isEmpty();

        if (changed) {
            return false;
        }

        for (Entry<InstanceField, InstanceField> entry:fieldsMatching.getMatched().entrySet()) {
            InstanceField oldField = entry.getKey();
            InstanceField newField = entry.getValue();
            JavaThing oldFieldValue = oldFieldValues[oldField.getIndex()];
            JavaThing newFieldValue = newFieldValues[newField.getIndex()];
            if (oldFieldValue == oldNull && newFieldValue == newNull) {
                continue;
            }
            if (oldFieldValue == oldNull || newFieldValue == newNull) {
                return false;
            }
            if (oldFieldValue instanceof JavaHeapObject) {
                JavaHeapObject oldJHO = (JavaHeapObject) oldFieldValue;
                JavaHeapObject newJHO = (JavaHeapObject) newFieldValue;

                if (!objectsMatching.isMatched(oldJHO, newJHO)) {
                    return false;
                }
            } else {
                if (!compareJavaValue((JavaValue)oldFieldValue, (JavaValue)newFieldValue)) {
                    return false;
                }
            }
        }
        return true;
    }     

    protected void printAll() {
        System.out.println(">>>>>>>>>>>>>>>>>>>>" + getClass().getSimpleName() + "<<<<<<<<<<<<<<<<<<<<");

        System.out.println("Matched Classes: " + classesMatching);

        System.out.println("Matched Classes: " + classesMatching);
        System.out.println("Matched Threads: " + threadsMatching);
        System.out.println("Matched Locals: "  + localsMatching);
        System.out.println("Matched Statics: " + staticFieldsMatching);
        System.out.println("Matched Objects: " + objectsMatching);
        System.out.println("Metric: " + objectsMatching.getMetric());
        printEI();
        System.out.println(">>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<");

    }

    @SuppressWarnings("unchecked")
    protected <E> E getJavaObjectField(JavaHeapObject object, String name) {
        return ((E)((JavaObject) object).getField(name));
    }

    protected void matchObjectsWithoutEI(LinkedList<JavaHeapObject> queue) {
        {
            Enumeration<JavaHeapObject> oldObjects = oldHeap.getSnapshot().getThings();
            while (oldObjects.hasMoreElements()) {
                JavaHeapObject o = oldObjects.nextElement(); 
                if (oldTrace.getAllocationEntryFromToPointer(o.getId(), oldHeap.getTimestamp()) == null) {
                    addToMapSet(oldObjectsWithoutEI, o.getClazz(), o);
                } else {
                    addToMapSet(oldObjectsWithEI, o.getClazz(), o);
                }
            }

            Enumeration<JavaHeapObject> newObjects = newHeap.getSnapshot().getThings();
            while (newObjects.hasMoreElements()) {
                JavaHeapObject n = newObjects.nextElement(); 
                if (newTrace.getAllocationEntryFromToPointer(n.getId(), newHeap.getTimestamp()) == null) {
                    addToMapSet(newObjectsWithoutEI, n.getClazz(), n);
                } else {
                    addToMapSet(newObjectsWithEI, n.getClazz(), n);
                }
            }
        }

        {
            JavaClass oldStringClazz = oldHeap.getClazz("java.lang.String");
            JavaClass newStringClazz = newHeap.getClazz("java.lang.String");
            Set<JavaHeapObject> oldObjects = oldObjectsWithoutEI.get(oldStringClazz);
            Set<JavaHeapObject> newObjects = newObjectsWithoutEI.get(newStringClazz);

            // TODO
            for (JavaHeapObject oldObject:oldObjects) {
                if (objectsMatching.isMatchedOld(oldObject)) {
                    continue;
                }
                String oldString = oldObject.toString();

                for (JavaHeapObject newObject:newObjects) {
                    if (objectsMatching.isMatchedNew(newObject)) {
                        continue;
                    }
                    String newString = newObject.toString();
                    if (oldString.equals(newString)) {
                        objectsMatching.addMatched(oldObject, newObject);
                        objectsMatching.addMatched((JavaHeapObject)getJavaObjectField(oldObject, "value"),
                                (JavaHeapObject)getJavaObjectField(newObject, "value"));
                        break;
                    }
                }
            }
        }

        {
            JavaClass oldClassClazz = oldHeap.getClazz("java.lang.Class");
            JavaClass newClassClazz = newHeap.getClazz("java.lang.Class");
            Set<JavaHeapObject> oldObjects = oldObjectsWithoutEI.get(oldClassClazz);
            Set<JavaHeapObject> newObjects = newObjectsWithoutEI.get(newClassClazz);

            // TODO
            for (JavaHeapObject oldObject:oldObjects) {
                if (objectsMatching.isMatchedOld(oldObject)) {
                    continue;
                }
                if (oldObject instanceof JavaClass) {
                    continue;
                }
                String oldString = oldObject.toString();

                for (JavaHeapObject newObject:newObjects) {
                    if (objectsMatching.isMatchedNew(newObject)) {
                        continue;
                    }
                    if (newObject instanceof JavaClass) {
                        continue;
                    }
                    String newString = newObject.toString();
                    if (oldString.equals(newString)) {
                        objectsMatching.addMatched(oldObject, newObject);
                        queue.add(oldObject);
                        queue.add(newObject);
                        break;
                    }
                }
            }
        }

        {
            JavaClass oldFieldClazz = oldHeap.getClazz("java.lang.reflect.Field");
            JavaClass newFieldClazz = newHeap.getClazz("java.lang.reflect.Field");
            Set<JavaHeapObject> oldObjects = oldObjectsWithoutEI.get(oldFieldClazz);
            Set<JavaHeapObject> newObjects = newObjectsWithoutEI.get(newFieldClazz);

            // TODO
            for (JavaHeapObject oldObject:oldObjects) {
                if (objectsMatching.isMatchedOld(oldObject)) {
                    continue;
                }
                JavaClass oldClazz = getJavaObjectField(oldObject, "clazz");
                String oldName = getJavaObjectField(oldObject, "name").toString();
                JavaHeapObject oldType = getJavaObjectField(oldObject, "type");
                for (JavaHeapObject newObject:newObjects) {
                    if (objectsMatching.isMatchedNew(newObject)) {
                        continue;
                    }
                    JavaClass newClazz = getJavaObjectField(newObject, "clazz");
                    if (!classesMatching.isMatched(oldClazz, newClazz)) {
                        continue;
                    }
                    JavaHeapObject newType = getJavaObjectField(newObject, "type");
                    if (!objectsMatching.isMatched(oldType, newType)) {
                        continue;
                    }
                    String newName = getJavaObjectField(newObject, "name").toString();
                    if (oldName.equals(newName)) {
                        objectsMatching.addMatched(oldObject, newObject);
                        queue.add(oldObject);
                        queue.add(newObject);
                        break;
                    }
                }
            }
        }


        {
            JavaClass oldMethodClazz = oldHeap.getClazz("java.lang.reflect.Method");
            JavaClass newMethodClazz = newHeap.getClazz("java.lang.reflect.Method");
            Set<JavaHeapObject> oldObjects = oldObjectsWithoutEI.get(oldMethodClazz);
            Set<JavaHeapObject> newObjects = newObjectsWithoutEI.get(newMethodClazz);

            // TODO
            for (JavaHeapObject oldObject:oldObjects) {
                if (objectsMatching.isMatchedOld(oldObject)) {
                    continue;
                }
                JavaClass oldClazz = getJavaObjectField(oldObject, "clazz");
                String oldName = getJavaObjectField(oldObject, "name").toString();
                JavaHeapObject oldType = getJavaObjectField(oldObject, "returnType");
                JavaHeapObject oldParameters = getJavaObjectField(oldObject, "parameterTypes");
                for (JavaHeapObject newObject:newObjects) {
                    if (objectsMatching.isMatchedNew(newObject)) {
                        continue;
                    }
                    JavaClass newClazz = getJavaObjectField(newObject, "clazz");
                    if (!classesMatching.isMatched(oldClazz, newClazz)) {
                        continue;
                    }
                    JavaHeapObject newType = getJavaObjectField(newObject, "returnType");
                    if (!objectsMatching.isMatched(oldType, newType)) {
                        continue;
                    }
                    String newName = getJavaObjectField(newObject, "name").toString();
                    if (!oldName.equals(newName)) {
                        continue;
                    }
                    JavaHeapObject newParameters = getJavaObjectField(newObject, "parameterTypes");
                    if (compareMethodParameterTypes(oldParameters, newParameters)) {
                        objectsMatching.addMatched(oldObject, newObject);
                        queue.add(oldObject);
                        queue.add(newObject);
                        break;
                    }
                }
            }
        }
        
        {
            JavaClass oldMethodClazz = oldHeap.getClazz("java.lang.reflect.Constructor");
            JavaClass newMethodClazz = newHeap.getClazz("java.lang.reflect.Constructor");
            Set<JavaHeapObject> oldObjects = oldObjectsWithoutEI.get(oldMethodClazz);
            Set<JavaHeapObject> newObjects = newObjectsWithoutEI.get(newMethodClazz);

            // TODO
            for (JavaHeapObject oldObject:oldObjects) {
                if (objectsMatching.isMatchedOld(oldObject)) {
                    continue;
                }
                JavaClass oldClazz = getJavaObjectField(oldObject, "clazz");
                JavaHeapObject oldParameters = getJavaObjectField(oldObject, "parameterTypes");
                for (JavaHeapObject newObject:newObjects) {
                    if (objectsMatching.isMatchedNew(newObject)) {
                        continue;
                    }
                    JavaClass newClazz = getJavaObjectField(newObject, "clazz");
                    if (!classesMatching.isMatched(oldClazz, newClazz)) {
                        continue;
                    }
                    JavaHeapObject newParameters = getJavaObjectField(newObject, "parameterTypes");
                    if (compareMethodParameterTypes(oldParameters, newParameters)) {
                        objectsMatching.addMatched(oldObject, newObject);
                        queue.add(oldObject);
                        queue.add(newObject);
                        break;
                    }
                }
            }
        }
    }

    private boolean compareMethodParameterTypes(JavaHeapObject oldParameters,
            JavaHeapObject newParameters) {
        JavaThing[] oldArray = ((JavaObjectArray) oldParameters).getElements();
        JavaThing[] newArray = ((JavaObjectArray) newParameters).getElements();
        if (oldArray.length != newArray.length) {
            return false;
        }

        // either a JavaClass for interface or class
        // or a JavaObject for class primitive types
        for (int i = 0; i < oldArray.length; i++) { 
            JavaHeapObject oldType = (JavaHeapObject) oldArray[i];
            JavaHeapObject newType = (JavaHeapObject) newArray[i];
            if (!objectsMatching.isMatched(oldType, newType)) {
                return false;
            }
        }

        return true;
    }

    static <K, V> void addToMapSet(Map<K, Set<V>> map, K key, V value) {
        Set<V> values = map.get(key);
        if (values == null) {
            values = new HashSet<V>();
            map.put(key, values);
        }
        values.add(value);
    }

    static <K, V> int sizeOfMapSet(Map<K, Set<V>> map) {
        int count = 0;
        for (Set<V> value : map.values()) {
            count += value.size();
        }
        return count;
    }

    static Comparator<Entry<JavaClass, Set<JavaHeapObject>>> comparator = new Comparator<Entry<JavaClass, Set<JavaHeapObject>>>() {

        @Override
        public int compare(Entry<JavaClass, Set<JavaHeapObject>> o1, Entry<JavaClass, Set<JavaHeapObject>> o2) {
            return o2.getValue().size() - o1.getValue().size();
        }
    };
    
    static void printHist(Map<JavaClass, Set<JavaHeapObject>> mapSet) {
        List<Entry<JavaClass, Set<JavaHeapObject>>> entries = new ArrayList<Entry<JavaClass, Set<JavaHeapObject>>>(mapSet.entrySet());

        Collections.sort(entries, comparator);

        int count = 0;
        for (Entry<JavaClass, Set<JavaHeapObject>> entry : entries) {
            System.out.format("%6d). %7d %s\n", count++, entry.getValue().size(), entry.getKey().getName());
        }
    }
    
    protected void printEI() {
        int oldTotal = objectsMatching.getOld().size() + objectsMatching.getMatched().size();
        int newTotal = objectsMatching.getNew().size() + objectsMatching.getMatched().size();
        int oldWithoutEICount = sizeOfMapSet(oldObjectsWithoutEI);
        int newWithoutEICount = sizeOfMapSet(newObjectsWithoutEI);

        System.out.println("There are " + oldWithoutEICount + " of " + oldTotal  + " objects in old snapshot without EI.");
        System.out.println("There are " + newWithoutEICount + " of " + newTotal  + " objects in new snapshot without EI.");

        if (printObjectsWithoutEI) {
            System.out.println("Old objects without EI:");
            printHist(oldObjectsWithoutEI);
            System.out.println("New objects without EI:");
            printHist(newObjectsWithoutEI);
        }
        if (printObjectsWithEI) {
            System.out.println("Old objects with EI:");
            printHist(oldObjectsWithEI);
            System.out.println("New objects with EI:");
            printHist(newObjectsWithEI);
        }
        
        Map<JavaClass, Set<JavaHeapObject>> oldWithEI = new HashMap<JavaClass, Set<JavaHeapObject>>();
        Map<JavaClass, Set<JavaHeapObject>> newWithEI = new HashMap<JavaClass, Set<JavaHeapObject>>();

        if (printObjects) System.out.println("Old objects:");
        int count = 0, withei = 0;
        for (JavaHeapObject oldObject : objectsMatching.getOld()) {
            boolean hasEI = oldTrace.getAllocationEntryFromToPointer(oldObject.getId(), oldHeap.getTimestamp()) != null;
            if (hasEI) {
                withei ++;
                addToMapSet(oldWithEI, oldObject.getClazz(), oldObject);
            }
            if (onlyWithEI && !hasEI) {
                continue;
            }
            if (printObjects) System.out.format("%6d).%s%s\n", count++, (hasEI ? " * " : "   "), oldObject);
        }
        System.out.println("Old objects with EI: " + withei);
        printHist(oldWithEI);
        if (printObjects) System.out.println("New objects:");
        count = 0;
        withei = 0;
        for (JavaHeapObject newObject : objectsMatching.getNew()) {
            boolean hasEI = newTrace.getAllocationEntryFromToPointer(newObject.getId(), newHeap.getTimestamp()) != null;
            if (hasEI) {
                withei ++;
                addToMapSet(newWithEI, newObject.getClazz(), newObject);
            }
            if (onlyWithEI && !hasEI) {
                continue;
            }
            if (printObjects) System.out.format("%6d).%s%s\n", count++, (hasEI ? " * " : "   "), newObject);
        }
        System.out.println("New objects with EI: " + withei);
        printHist(newWithEI);
    }

    /**
     * Match thread via their creation order, i.e., tid
     */
    protected void matchThreads() {
        for (JavaThread oldJT : oldHeap.getThreads()) {
            JavaThing oldTID = oldJT.getJavaObject().getField("tid");
            JavaThread newJT = null;
            for (JavaThread temp : newHeap.getThreads()) {
                JavaThing newTID = temp.getJavaObject().getField("tid");
                if (oldTID.toString().equals(newTID.toString())) {
                    newJT = temp;
                    break;
                }
            }

            if (newJT != null) {
                threadsMatching.addMatched(oldJT, newJT);
                matchFrames(oldJT, newJT);
            } else {
                threadsMatching.addOld(oldJT);
            }
        }

        for (JavaThread newJT : newHeap.getThreads()) {
            if (!threadsMatching.getMatchedReverse().containsKey(newJT)) {
                threadsMatching.addNew(newJT);
            }
        }
    }

    public static String printStackFrame(StackFrame sf) {
        return String.format("%s.%s%s@%s", sf.getClassName(), sf.getMethodName(), sf.getMethodSignature(), sf.getLineNumber());
    }

    protected void matchFrames(JavaThread oldJT, JavaThread newJT) {
        Frame[] oldFrames = oldJT.getFrames();
        Frame[] newFrames = newJT.getFrames();

        int oldLength = oldFrames.length;
        int newLength = newFrames.length;

        int length = oldLength < newLength ? oldLength: newLength;

        int i;
        for (i=0; i<length;i++) {
            Frame oldFrame = oldFrames[oldLength - 1 - i];
            Frame newFrame = newFrames[newLength - 1 - i];
            if (!isMatched(oldFrame.getStackFrame(), newFrame.getStackFrame())) {
                System.err.println("WARNING: unmatched frames: " + 
                        printStackFrame(oldFrame.getStackFrame()) + " " + 
                        printStackFrame(newFrame.getStackFrame()));
                for (LocalReference lr:oldFrame.getLocals()) {
                    localsMatching.addOld(lr);
                }
                for (LocalReference lr:newFrame.getLocals()) {
                    localsMatching.addNew(lr);
                }
                continue;
            }

            LocalReference[] oldLocals = oldFrame.getLocals();
            LocalReference[] newLocals = newFrame.getLocals();
            if (oldLocals.length != newLocals.length) {
                throw new RuntimeException("Oops");
            }

            for (int j=0; j<oldLocals.length; j++) {
                localsMatching.addMatched(oldLocals[j], newLocals[j]);
            }
        }

        for (int j=i; j<oldLength; j++) {
            Frame oldFrame = oldFrames[oldLength - 1 - i];
            for (LocalReference lr:oldFrame.getLocals()) {
                localsMatching.addOld(lr);
            }
        }

        for (int j=i; j<newLength; j++) {
            Frame newFrame = newFrames[newLength - 1 - i];
            for (LocalReference lr:newFrame.getLocals()) {
                localsMatching.addNew(lr);
            }
        }
    }

    /**
     * Assume that all snapshots is captured at DSU safe points w.r.t. activeness safety 
     * @param oldSF
     * @param newSF
     * @return
     */
    boolean isMatched(StackFrame oldSF, StackFrame newSF) {
        if (oldSF.getClassName().equals(newSF.getClassName())) {
            if (oldSF.getMethodName().equals(newSF.getMethodName())) {
                if (oldSF.getMethodSignature().equals(newSF.getMethodSignature())) {
                    if (oldSF.getLineNumber().equals(newSF.getLineNumber())) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    protected void matchClasses() {
        Iterator<JavaClass> it = oldHeap.getClasses();
        while (it.hasNext()) {
            JavaClass oldClass = it.next();
            JavaClass newClass = newHeap.getClazz(oldClass.getName());
            if (newClass == null) {
                classesMatching.addOld(oldClass);
                objectsMatching.addOld(oldClass);
            } else {
                classesMatching.addMatched(oldClass, newClass);
                objectsMatching.addMatched(oldClass, newClass);
            }
        }

        it = newHeap.getClasses();
        while (it.hasNext()) {
            JavaClass newClass = it.next();
            JavaClass oldClass = oldHeap.getClazz(newClass.getName());
            if (oldClass == null) {
                classesMatching.addNew(newClass);
                objectsMatching.addNew(newClass);
            }
        }
    }

    protected void matchStatics() {
        for (StaticFieldReference sfr:oldHeap.getStaticFields().values()) {
            String id = sfr.getIdentity();
            StaticFieldReference sfrNew = newHeap.getStaticField(id);
            if (sfrNew == null) {
                staticFieldsMatching.addOld(sfr);
            } else {
                staticFieldsMatching.addMatched(sfr, sfrNew);
            }
        }

        for (StaticFieldReference sfr:newHeap.getStaticFields().values()) {
            String id = sfr.getIdentity();
            StaticFieldReference sfrOld = oldHeap.getStaticField(id);
            if (sfrOld == null) {
                staticFieldsMatching.addNew(sfr);
            } else {
                if (staticFieldsMatching.getMatched().get(sfrOld) != sfr) {
                    throw new RuntimeException("Sanity check failed!");
                }
            }
        }
    }

    protected void matchObjects() {
        LinkedList<JavaHeapObject> queue = new LinkedList<JavaHeapObject>();
        for (Entry<StaticFieldReference, StaticFieldReference> entry:staticFieldsMatching.getMatched().entrySet()) {
            StaticFieldReference oldStatic = entry.getKey();
            StaticFieldReference newStatic = entry.getValue();
            JavaHeapObject oldObject = oldHeap.getPointedTo(oldStatic);
            JavaHeapObject newObject = newHeap.getPointedTo(newStatic);
            if (!compareAndAppendIntoQueue(queue, oldObject, newObject)) {
                staticFieldsMatching.markChanged(oldStatic, newStatic);
            }
        }

        for (Entry<LocalReference, LocalReference> entry:localsMatching.getMatched().entrySet()) {
            LocalReference oldLocal = entry.getKey();
            LocalReference newLocal = entry.getValue();
            JavaHeapObject oldObject = oldHeap.getPointedTo(oldLocal);
            JavaHeapObject newObject = newHeap.getPointedTo(newLocal);
            if (!compareAndAppendIntoQueue(queue, oldObject, newObject)) {
                localsMatching.markChanged(oldLocal, newLocal);
            }
        }

        for (Entry<JavaThread, JavaThread> entry:threadsMatching.getMatched().entrySet()) {
            JavaThread oldJT = entry.getKey();
            JavaThread newJT = entry.getValue();
            JavaHeapObject oldObject = oldJT.getJavaObject();
            JavaHeapObject newObject = newJT.getJavaObject();
            objectsMatching.addMatched(oldObject, newObject);
            queue.add(oldObject);
            queue.add(newObject);
        }

        Set<JavaHeapObject> visited = new HashSet<JavaHeapObject>();

        matchObjectsWithoutEI(queue);

        while(!queue.isEmpty()) { // every pair objects in the queue are matched.
            JavaHeapObject oldObject = queue.removeFirst();
            JavaHeapObject newObject = queue.removeFirst();

            if (visited.contains(oldObject) && visited.contains(newObject)) {
                continue;
            }

            JavaClass oldClass = oldObject.getClazz();
            JavaClass newClass = newObject.getClazz();

            if (!classesMatching.isMatched(oldClass, newClass)) {
                // Different object
                continue;
            }

            if (oldObject instanceof JavaObject) {
                JavaObject oldJO = (JavaObject) oldObject;
                JavaObject newJO = (JavaObject) newObject;
                matchAndVisitJavaObject(queue, oldJO, newJO, oldClass, newClass);
            } else if (oldObject instanceof JavaValueArray) {
                JavaValueArray oldJVA = (JavaValueArray) oldObject;
                JavaValueArray newJVA = (JavaValueArray) newObject;
                matchAndVisitJavaValueArray(queue, oldJVA, newJVA, oldClass, newClass);
            } else if (oldObject instanceof JavaObjectArray) {
                JavaObjectArray oldJOA = (JavaObjectArray) oldObject;
                JavaObjectArray newJOA = (JavaObjectArray) newObject;
                matchAndVisitJavaObjectArray(queue, oldJOA, newJOA, oldClass, newClass);
            } else if (oldObject instanceof JavaClass) {
                JavaClass oldClazz = (JavaClass) oldObject;
                JavaClass newClazz = (JavaClass) newObject;
                matchAndVisitJavaClass(queue, oldClazz, newClazz);
            } else {
                System.err.println("Unsupported class: " + oldClass.getName());
                System.err.println("Unsupported class: " + newClass.getName());
            }

            visited.add(oldObject);
            visited.add(newObject);
        }

        if (onlyStatic) {
            for (JavaHeapObject oldObject:oldHeap.getReachableFromStatics()) {
                if (!objectsMatching.getMatched().containsKey(oldObject)) {
                    objectsMatching.addOld(oldObject);
                }
            }
            for (JavaHeapObject newObject:newHeap.getReachableFromStatics()) {
                if (!objectsMatching.getMatchedReverse().containsKey(newObject)) {
                    objectsMatching.addNew(newObject);
                }
            }
        } else {
            collectUntouchedObjects();
        }

    }

    protected void collectUntouchedObjects() {
        Enumeration<JavaHeapObject> oldObjects = oldHeap.getSnapshot().getThings();
        while (oldObjects.hasMoreElements()) {
            JavaHeapObject oldObject = oldObjects.nextElement();
            if (!objectsMatching.isMatchedOld(oldObject)) {
                objectsMatching.addOld(oldObject);
            }
        }

        Enumeration<JavaHeapObject> newObjects = newHeap.getSnapshot().getThings();
        while (newObjects.hasMoreElements()) {
            JavaHeapObject newObject = newObjects.nextElement();
            if (!objectsMatching.isMatchedNew(newObject)) {
                objectsMatching.addNew(newObject);
            }
        }
    }

    protected JavaHeapObject findMatchedNewObjectByAllocationEntry(JavaHeapObject oldObject, AllocationEntry oldAE) {
        JavaThread oldThread = oldHeap.getThreadByTrace(oldAE.getThreadTrace());

        // 1). get matched thread
        JavaThread newThread = threadsMatching.getMatched().get(oldThread);
        if (newThread == null) {
            throw new RuntimeException("Oops, not implemented");
        }

        // 2). get new thread trace
        ThreadTrace newTT = newTrace.getThreadTraceByEETOP(Long.valueOf(newThread.getJavaObject().getField("eetop").toString()));
        if (newTT == null) {
            throw new RuntimeException("Oops, not implemented");
        }
        
        // 3). get new allocation entry
        AllocationEntry newAE = newTT.getAllocationEntry(oldAE);
        if (newAE == null) {
            return null;
        }

        // 4). find the final `to` of this from
        long time = newAE.getObject().getTimestamp();
        long fromID = newAE.getObject().getId(); // a born id is a fromID
        long[] timestamps = newTrace.getTimeline().getTimestamps();

        // 5). find the born time stamp
        int i = Arrays.binarySearch(timestamps, time);
        if (i == -1) {
            throw new RuntimeException("Oops...");
        }

        long stopTimestamp = newHeap.getTimestamp();
        // last is Long.MAX_VALUE
        // TODO: we assume that the last GC is for dumping
        for ( ; i < timestamps.length -1; i++) {
            time = timestamps[i];
            ForwardPointers fp = newTrace.getForwardPointers().get(time);
            if (fp == null) {
                throw new RuntimeException("Oops");
            }
            long toID = fp.getTo(fromID);
            if (toID == 0) {
                break;
            }
            fromID = toID;
            if (time == stopTimestamp) {
                break;
            }
        }
        
        // 3) query the object from the toID (latest fromID for next potential query)
        JavaHeapObject newObject = newHeap.getSnapshot().findThing(fromID);
        // newObject may be collected
        
        if (newObject == null) {
            //throw new RuntimeException("A new object has been collected...");
            return null;
        }

        JavaClass oldClazz = oldObject.getClazz();

        if (newTrace.getAllocationEntryFromToPointer(newObject.getId(), newHeap.getTimestamp()) != newAE) {
            System.out.println(oldAE);
            System.out.println(newAE);
            System.out.println("fromID=" + fromID + ", new object id" + newObject.getId() + ", timestamp=" + newHeap.getTimestamp());
            System.out.println(newTrace.getAllocationEntryFromToPointer(newObject.getId(), newHeap.getTimestamp()));
            newTrace.getAllocationEntryFromToPointer(newObject.getId(), newHeap.getTimestamp());
            throw new RuntimeException("Sanity check failed");
        }

        // 4) may allocate different types of objects
        JavaClass newClazz = newObject.getClazz();
        if (!classesMatching.isMatched(oldClazz, newClazz)) {
            //throw new RuntimeException("Oops");
            return null;
        }

        return newObject;
    }

    protected JavaHeapObject findMatchedOldObjectByAllocationEntry(JavaHeapObject newObject, AllocationEntry newAE) {
        JavaThread newThread = newHeap.getThreadByTrace(newAE.getThreadTrace());
        if (newThread == null) {
            throw new RuntimeException("Oops, not implemented");
        }
        // 1). get matched thread
        JavaThread oldThread = threadsMatching.getMatchedReverse().get(newThread);
        if (oldThread == null) {
            throw new RuntimeException("Oops, not implemented");
        }

        // 2). get old thread trace
        ThreadTrace oldTT = oldTrace.getThreadTraceByEETOP(Long.valueOf(oldThread.getJavaObject().getField("eetop").toString()));
        if (oldTT == null) {
            throw new RuntimeException("Oops, not implemented");
        }
        
        // 3). get old allocation entry
        AllocationEntry oldAE = oldTT.getAllocationEntry(newAE);
        if (oldAE == null) {
            return null;
        }

        // 4). find the final `to` of this from
        long time = oldAE.getObject().getTimestamp();
        long fromID = oldAE.getObject().getId(); // a born id is a fromID
        long[] timestamps = oldTrace.getTimeline().getTimestamps();

        // 5). find the born time stamp
        int i = Arrays.binarySearch(timestamps, time);
        if (i == -1) {
            throw new RuntimeException("Oops...");
        }

        long stopTimestamp = oldHeap.getTimestamp();
        // last is Long.MAX_VALUE
        // TODO: we assume that the last GC is for dumping
        for ( ; i < timestamps.length -1; i++) {
            time = timestamps[i];
            ForwardPointers fp = oldTrace.getForwardPointers().get(time);
            if (fp == null) {
                throw new RuntimeException("Oops");
            }
            long toID = fp.getTo(fromID);
            if (toID == 0) {
                break;
            }
            fromID = toID;
            if (time == stopTimestamp) {
                break;
            }
        }
        
        // 3) query the object from the toID (latest fromID for next potential query)
        JavaHeapObject oldObject = oldHeap.getSnapshot().findThing(fromID);
        // newObject may be collected
        
        if (oldObject == null) {
            return null;
        }

        JavaClass newClazz = newObject.getClazz();

        if (oldTrace.getAllocationEntryFromToPointer(oldObject.getId(), oldHeap.getTimestamp()) != oldAE) {
            throw new RuntimeException("Sanity check failed");
        }

        // 4) may allocate different types of objects
        JavaClass oldClazz = oldObject.getClazz();
        if (!classesMatching.isMatched(oldClazz, newClazz)) {
            return null;
        }

        return oldObject;
    }
    
    private void matchAndVisitJavaClass(LinkedList<JavaHeapObject> queue,
            JavaClass oldClazz, JavaClass newClazz) {
        // do nothing
    }

    protected void matchAndVisitJavaObjectArray(LinkedList<JavaHeapObject> queue,
            JavaObjectArray oldJOA, JavaObjectArray newJOA, JavaClass oldClass,
            JavaClass newClass) {
        final int oldLength = oldJOA.getLength();
        final int newLength = newJOA.getLength();

        final int length = oldLength < newLength ? oldLength: newLength;
        JavaThing[] oldElements = oldJOA.getElements();
        JavaThing[] newElements = newJOA.getElements();
        for (int i=0; i<length; i++) {
            JavaHeapObject oldE = (JavaHeapObject) oldElements[i];
            JavaHeapObject newE = (JavaHeapObject) newElements[i];
            if (oldE == null && newE == null) {
                continue;
            }
            if (oldE == oldNull || newE == newNull) {
                throw new RuntimeException("Oops...");
            }
            if (oldE != null && newE != null) {
                compareAndAppendIntoQueue(queue, oldE, newE);
            }
        }
    }

    protected void matchAndVisitJavaValueArray(LinkedList<JavaHeapObject> queue,
            JavaValueArray oldJVA, JavaValueArray newJVA, JavaClass oldClass,
            JavaClass newClass) {

    }

    protected boolean compareValueArray(JavaValueArray oldJVA, JavaValueArray newJVA) {
        final int len = oldJVA.getLength();
        if (newJVA.getLength() != len) {
            return false;
        }
        final byte et = oldJVA.getElementType();
        switch (et) {
        case 'Z': {
            for (int i = 0; i < len; i++) {
                boolean o = oldJVA.getBooleanAt(i);
                boolean n = newJVA.getBooleanAt(i);
                if (o != n) {
                    return false;
                }
            }
            return true;
        }
        case 'B': {
            for (int i = 0; i < len; i++) {
                byte o = oldJVA.getByteAt(i);
                byte n = newJVA.getByteAt(i);
                if (o != n) {
                    return false;
                }
            }
            return true;
        }
        case 'C': {
            for (int i = 0; i < len; i++) {
                char o = oldJVA.getCharAt(i);
                char n = newJVA.getCharAt(i);
                if (o != n) {
                    return false;
                }
            }
            return true;
        }
        case 'S': {
            for (int i = 0; i < len; i++) {
                short o = oldJVA.getShortAt(i);
                short n = newJVA.getShortAt(i);
                if (o != n) {
                    return false;
                }
            }
            return true;
        }
        case 'I': {
            for (int i = 0; i < len; i++) {
                int o = oldJVA.getIntAt(i);
                int n = newJVA.getIntAt(i);
                if (o != n) {
                    return false;
                }
            }
            return true;
        }
        case 'J': {
            for (int i = 0; i < len; i++) {
                long o = oldJVA.getLongAt(i);
                long n = newJVA.getLongAt(i);
                if (o != n) {
                    return false;
                }
            }
            return true;
        }
        case 'F': {
            for (int i = 0; i < len; i++) {
                float o = oldJVA.getFloatAt(i);
                float n = newJVA.getFloatAt(i);
                if (o != n) {
                    return false;
                }
            }
            return true;
        }
        case 'D': {
            for (int i = 0; i < len; i++) {
                double o = oldJVA.getDoubleAt(i);
                double n = newJVA.getDoubleAt(i);
                if (o != n) {
                    return false;
                }
            }
            return true;
        }
        default: {
            throw new RuntimeException("unknown primitive type?");
        }
        }
    }


    /**
     * 
     * @param queue
     * @param oldJHO
     * @param newJHO
     * @return true if these two objects can be matched
     */
    protected boolean compareAndAppendIntoQueue(LinkedList<JavaHeapObject> queue, JavaHeapObject oldJHO, JavaHeapObject newJHO) {
        if (oldJHO == null || newJHO == null) {
            return false;
        }

        if (oldJHO == oldNull || newJHO == newNull) {
            return false;
        }

        if (oldJHO.getClass() != newJHO.getClass()) {
            return false;
        }
        
        if (objectsMatching.isOld(oldJHO) || objectsMatching.isNew(newJHO)) {
            return false;
        }
        // TODO: use classesMatching
        if (!oldJHO.getClazz().getName().equals(newJHO.getClazz().getName())) {
            return false;
        }

        // duplicated matching
        if (objectsMatching.addMatched(oldJHO, newJHO)) {
            queue.add(oldJHO);
            queue.add(newJHO);
            return true;
        }

        return false;
    }

    protected void matchAndVisitJavaObject(LinkedList<JavaHeapObject> queue,
            JavaObject oldJO, JavaObject newJO, JavaClass oldClass,
            JavaClass newClass) {
        Matching<InstanceField> fieldsMatching = getMatchedInstanceFields(oldClass, newClass);
        JavaThing[] oldFieldValues = oldJO.getFields();
        JavaThing[] newFieldValues = newJO.getFields();

        for (Entry<InstanceField, InstanceField> entry:fieldsMatching.getMatched().entrySet()) {
            InstanceField oldField = entry.getKey();
            InstanceField newField = entry.getValue();
            JavaThing oldFieldValue = oldFieldValues[oldField.getIndex()];
            JavaThing newFieldValue = newFieldValues[newField.getIndex()];
            if (oldFieldValue == oldNull && newFieldValue == newNull) {
                continue;
            }
            if (oldFieldValue == oldNull || newFieldValue == newNull) {
                continue;
            }
            if (oldFieldValue instanceof JavaHeapObject) {
                JavaHeapObject oldJHO = (JavaHeapObject) oldFieldValue;
                JavaHeapObject newJHO = (JavaHeapObject) newFieldValue;

                compareAndAppendIntoQueue(queue, oldJHO, newJHO);
            }
        }
    }

    /**
     * 
     * @param oldValue
     * @param newValue
     * @return true if the two value are literally same
     */
    protected boolean compareJavaValue(JavaValue oldValue, JavaValue newValue) {
        return oldValue.toString().equals(newValue.toString());
    }

    protected Matching<InstanceField> getMatchedInstanceFields(JavaClass oldClass, JavaClass newClass) {
        if (oldClass == null || newClass == null) {
            return null;
        }

        Matching<InstanceField> fieldsMatching = oldClassToMatchedFields.get(oldClass);
        if (fieldsMatching != null) {
            return fieldsMatching;
        }

        fieldsMatching = new Matching<InstanceField>();
        oldClassToMatchedFields.put(oldClass, fieldsMatching);

        // only matched classes can reach here.
        if (!classesMatching.isMatched(oldClass, newClass)) {
            throw new RuntimeException("Oops");
        }

        InstanceField[] oldInstanceFields = oldHeap.getInstanceFields(oldClass);
        InstanceField[] newInstanceFields = newHeap.getInstanceFields(newClass);
        for (InstanceField oldField:oldInstanceFields) {
            for (InstanceField newField:newInstanceFields) {
                if (oldField.getDeclaringClass().equals(newField.getDeclaringClass())
                        && oldField.getName().equals(newField.getName())
                        && oldField.getSignature().equals(newField.getSignature())) {
                    fieldsMatching.addMatched(oldField, newField);
                }
            }
        }

        for (InstanceField oldField:oldInstanceFields) {
            if (!fieldsMatching.isMatchedOld(oldField)) {
                fieldsMatching.addOld(oldField);
            }
        }

        for (InstanceField newField:newInstanceFields) {
            if (!fieldsMatching.isMatchedNew(newField)) {
                fieldsMatching.addNew(newField);
            }
        }

        return fieldsMatching;
    }


    protected boolean compareAllocationEntry(AllocationEntry oldAE,
            AllocationEntry newAE) {
        if (oldAE.getBci() == -1) {
            throw new RuntimeException();
        }

        JavaThread oldJavaThread = oldHeap.getThreadByTrace(oldAE.getThreadTrace());
        JavaThread newJavaThread = newHeap.getThreadByTrace(newAE.getThreadTrace());

        if (!threadsMatching.isMatched(oldJavaThread, newJavaThread)) {
            threadsMatching.isMatched(oldJavaThread, newJavaThread);
            return false;
        }

        CallEntry oldCallerEntry = oldAE.getParentEntry();
        CallEntry newCallerEntry = newAE.getParentEntry();
        if (oldCallerEntry == null && newCallerEntry != null) {
            return false;
        }

        if (oldCallerEntry != null && newCallerEntry == null) {
            return false;
        }

        if (oldCallerEntry != null && newCallerEntry != null) {
            if (!compareCallEntry(oldCallerEntry, newCallerEntry)) {
                return false;
            }
        }

        if (oldAE.getBci() != newAE.getBci()) {
            return false;
        }

        if (oldAE.getCount() != newAE.getCount()) {
            return false;
        }

        return true;
    }

    protected boolean compareCallEntry(CallEntry oldEntry,
            CallEntry newEntry) {
        if (oldEntry == null && newEntry == null) {
            return true;
        }
        if (oldEntry == null || newEntry == null) {
            return false;
        }
        if (oldEntry.getDepth() != newEntry.getDepth()) {
            return false;
        }
        if (!compareMethodMeta(oldEntry.getMethod(), newEntry.getMethod())) {
            return false;
        }
        if (oldEntry.getCallerBci() != newEntry.getCallerBci()) {
            return false;
        }
        if (oldEntry.getCount() != newEntry.getCount()) {
            return false;
        }
        return compareCallEntry(oldEntry.getParent(), newEntry.getParent());
    }

    /**
     * return true if equal
     * @param oldMethod
     * @param newMethod
     * @return
     */
    protected boolean compareMethodMeta(MethodMeta oldMethod, MethodMeta newMethod) {
        return methodComparator.compareMethodMeta(oldMethod, newMethod);
    }
}

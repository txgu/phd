package org.javelus.hprof.comparator;

import com.sun.tools.hat.internal.model.StackFrame;

public class Frame {
    private LocalReference[] locals;
    private StackFrame stackFrame;
    public Frame(int count, StackFrame stackFrame) {
        locals = new LocalReference[count];
        this.stackFrame = stackFrame;
    }

    public void setLocal(int slot, LocalReference lr) {
        locals[slot] = lr;
    }

    public StackFrame getStackFrame() {
        return stackFrame;
    }

    public LocalReference[] getLocals() {
        return locals;
    }
}

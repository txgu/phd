package org.javelus.hprof.comparator;

import java.util.HashSet;
import java.util.Set;

import org.javelus.DSU;
import org.javelus.DSUClass;
import org.javelus.DSUClassLoader;
import org.javelus.DSUMethod;
import org.javelus.minitracing.MethodMeta;

public class DSUMethodMetaComparator implements MethodMetaComparator {

    private DSU dsu;

    private Set<MethodMeta> bodyChangedMethods = new HashSet<MethodMeta>();

    public DSUMethodMetaComparator(DSU dsu) {
        this.dsu = dsu;
        buildBodyChangedMethods(dsu);
    }

    private void buildBodyChangedMethods(DSU dsu2) {
        for (DSUClassLoader loader : dsu.getClassLoaders()) {
            for (DSUClass cls : loader.getDSUClasses()) {
                for (DSUMethod method : cls.getDSUMethods()) {
                    if (method.getUpdateType().isChanged()) {
                        MethodMeta meta = new MethodMeta(cls.getName(), method.getName(), method.getSignature());
                        bodyChangedMethods.add(meta);
                    }
                }
            }
        }
    }

    @Override
    public boolean compareMethodMeta(MethodMeta oldMethod,
            MethodMeta newMethod) {
        return oldMethod.equals(newMethod) && compareMethodBody(oldMethod, newMethod);
    }

    /**
     * return true if equal
     * @param oldMethod
     * @param newMethod
     * @return
     */
    private boolean compareMethodBody(MethodMeta oldMethod,
            MethodMeta newMethod) {
        return !bodyChangedMethods.contains(oldMethod);
    }

}

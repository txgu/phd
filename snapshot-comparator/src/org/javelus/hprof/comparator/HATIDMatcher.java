package org.javelus.hprof.comparator;

import java.util.Enumeration;

import org.javelus.minitracing.ThreadTraces;

import com.sun.tools.hat.internal.model.JavaClass;
import com.sun.tools.hat.internal.model.JavaHeapObject;

public class HATIDMatcher extends Matcher {

    public HATIDMatcher(ObjectGraph oldHeap, ObjectGraph newHeap,  ThreadTraces oldTrace, ThreadTraces newTrace, MethodMetaComparator comparator) {
        super(oldHeap, newHeap, oldTrace, newTrace, comparator);
    }

    @Override
    protected void matchObjects() {
        Enumeration<JavaHeapObject> oldObjects = oldHeap.getSnapshot().getThings();
        while (oldObjects.hasMoreElements()) {
            JavaHeapObject oldObject = oldObjects.nextElement();
            JavaHeapObject newObject = newHeap.getSnapshot().findThing(oldObject.getId());
            if (newObject != null) {
                if (oldObject.getClass() != newObject.getClass()) {
                    objectsMatching.addOld(oldObject);
                    objectsMatching.addNew(newObject);
                    continue;
                }

                JavaClass oldClass = oldObject.getClazz();
                JavaClass newClass = newObject.getClazz();

                if (classesMatching.isMatched(oldClass, newClass)) {
                    objectsMatching.addMatched(oldObject, newObject);
                } else {
                    objectsMatching.addOld(oldObject);
                    objectsMatching.addNew(newObject);
                }
            } else {
                objectsMatching.addOld(oldObject);
            }
        }

        Enumeration<JavaHeapObject> newObjects = newHeap.getSnapshot().getThings();
        while (newObjects.hasMoreElements()) {
            JavaHeapObject newObject = newObjects.nextElement();
            JavaHeapObject oldObject = oldHeap.getSnapshot().findThing(newObject.getId());
            if (oldObject == null) {
                objectsMatching.addNew(newObject);
            } else {
                JavaClass oldClass = oldObject.getClazz();
                JavaClass newClass = newObject.getClazz();

                if (!classesMatching.isMatched(oldClass, newClass)) {
                    objectsMatching.addNew(newObject);
                }
            }
        }
    }


}

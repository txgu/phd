package org.javelus.hprof.comparator;

import java.util.Enumeration;
import java.util.LinkedList;

import org.javelus.minitracing.AllocationEntry;
import org.javelus.minitracing.ThreadTraces;

import com.sun.tools.hat.internal.model.JavaClass;
import com.sun.tools.hat.internal.model.JavaHeapObject;

public class CallStringIDMatcher extends Matcher {

    public CallStringIDMatcher(ObjectGraph oldHeap, ObjectGraph newHeap,  ThreadTraces oldTrace, ThreadTraces newTrace, MethodMetaComparator comparator) {
        super(oldHeap, newHeap, oldTrace, newTrace, comparator);
    }


    @Override
    protected void matchObjects() {
        matchObjectsWithoutEI(new LinkedList<JavaHeapObject>());

        Enumeration<JavaHeapObject> oldObjects = oldHeap.getSnapshot().getThings();
        while (oldObjects.hasMoreElements()) {
            JavaHeapObject oldObject = oldObjects.nextElement();
            if (objectsMatching.isMatchedOld(oldObject)) {
                continue;
            }
            AllocationEntry oldAE = oldTrace.getAllocationEntryFromToPointer(oldObject.getId(), oldHeap.getTimestamp());
            if (oldAE == null) {
                objectsMatching.addOld(oldObject);
                continue;
            }
            JavaHeapObject newObject = findMatchedNewObjectByAllocationEntry(oldObject, oldAE);
            if (newObject != null) {
                if (oldObject.getClass() != newObject.getClass()) {
                    objectsMatching.addOld(oldObject);
                    continue;
                }

                JavaClass oldClass = oldObject.getClazz();
                JavaClass newClass = newObject.getClazz();

                if (classesMatching.isMatched(oldClass, newClass)) {
                    if (!objectsMatching.addMatched(oldObject, newObject)) {
                        objectsMatching.addOld(oldObject);
                    }
                } else {
                    objectsMatching.addOld(oldObject);
                }
            } else {
                objectsMatching.addOld(oldObject);
            }
        }

        Enumeration<JavaHeapObject> newObjects = newHeap.getSnapshot().getThings();
        while (newObjects.hasMoreElements()) {
            JavaHeapObject newObject = newObjects.nextElement();
            if (objectsMatching.isMatchedNew(newObject)) {
                continue;
            }
            AllocationEntry newAE = newTrace.getAllocationEntryFromToPointer(newObject.getId(), newHeap.getTimestamp());
            if (newAE == null) {
                objectsMatching.addNew(newObject);
                continue;
            }
            if (!objectsMatching.isMatchedNew(newObject)) {
                objectsMatching.addNew(newObject);
            }
        }

    }


}

package org.javelus.hprof.comparator;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

import org.javelus.impl.DSUSpecReader;
import org.javelus.minitracing.ThreadTraces;

import com.sun.tools.hat.internal.model.Snapshot;
import com.sun.tools.hat.internal.parser.Reader;

public class Main {

    public static Snapshot readSnapshot(String file) {
        Snapshot snapshot = null; 
        try {
            snapshot = Reader.readFile(file, true, 0);
            snapshot.resolve(true);
        } catch (IOException e) {
            throw new RuntimeException("Cannot read snapshot in " + file);
        }
        return snapshot;
    }

    public static void main(String[] argv) throws Exception {
        Properties prop = new Properties();
        prop.load(new FileInputStream(argv[0]));

        Path oldDataDir = Paths.get(prop.getProperty("old.datadir"));
        Path newDataDir = Paths.get(prop.getProperty("new.datadir"));

        Snapshot oldSnapshot = readSnapshot(oldDataDir.resolve(prop.getProperty("old.snapshot")).toString());
        Snapshot newSnapshot = readSnapshot(newDataDir.resolve(prop.getProperty("new.snapshot")).toString());
        ThreadTraces oldTrace = ThreadTraces.create(false, oldDataDir.toFile());
        ThreadTraces newTrace = ThreadTraces.create(false, newDataDir.toFile());

        long oldTimestamp = Long.parseUnsignedLong(prop.getProperty("old.timestamp"), 16);
        long newTimestamp = Long.parseUnsignedLong(prop.getProperty("new.timestamp"), 16);

        ObjectGraph oldHeap = new ObjectGraph(oldSnapshot, oldTimestamp);
        ObjectGraph newHeap = new ObjectGraph(newSnapshot, newTimestamp);

        MethodMetaComparator comparator = null;
        String dsupath = prop.getProperty("dsupath");
        if (dsupath != null) {
            comparator = new DSUMethodMetaComparator(DSUSpecReader.read(dsupath));
        }
        {
            Matcher matcher = new Matcher(oldHeap, newHeap, oldTrace, newTrace, comparator);
            matcher.match();
        }

        {
            Matcher matcher = new CallStringIDMatcher(oldHeap, newHeap, oldTrace, newTrace, comparator);
            matcher.match();
        }

        {
            Matcher matcher = new CallStringMatcher(oldHeap, newHeap, oldTrace, newTrace, comparator);
            matcher.match();
        }
//        
//        {
//            Matcher matcher = new CallStringMatcher2(oldHeap, newHeap, oldTrace, newTrace, comparator);
//            matcher.match();
//        }
//        
//        {
//            Matcher matcher = new CallStringMatcher3(oldHeap, newHeap, oldTrace, newTrace, comparator);
//            matcher.match();
//        }
    }
}

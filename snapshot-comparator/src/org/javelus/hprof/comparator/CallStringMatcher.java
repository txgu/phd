package org.javelus.hprof.comparator;

import java.util.Enumeration;
import java.util.LinkedList;

import org.javelus.minitracing.AllocationEntry;
import org.javelus.minitracing.ThreadTraces;

import com.sun.tools.hat.internal.model.JavaClass;
import com.sun.tools.hat.internal.model.JavaHeapObject;
import com.sun.tools.hat.internal.model.JavaObjectArray;
import com.sun.tools.hat.internal.model.JavaThing;

public class CallStringMatcher extends Matcher {


    int unmatchedIndexCount = 0;
    int matchedIndexCount = 0;
    int untouchedMatchedCount = 0;
    public CallStringMatcher(ObjectGraph oldHeap, ObjectGraph newHeap, ThreadTraces oldTrace, ThreadTraces newTrace, MethodMetaComparator comparator) {
        super(oldHeap, newHeap, oldTrace, newTrace, comparator);

    }

    protected void matchAndVisitJavaObjectArray(LinkedList<JavaHeapObject> queue,
            JavaObjectArray oldJOA, JavaObjectArray newJOA, JavaClass oldClass,
            JavaClass newClass) {
        final int oldLength = oldJOA.getLength();
        final int newLength = newJOA.getLength();

        JavaThing[] oldElements = oldJOA.getElements();
        JavaThing[] newElements = newJOA.getElements();
        for (int i=0; i<oldLength; i++) {
            JavaHeapObject oldE = (JavaHeapObject) oldElements[i];
            if (oldE == null) {
                continue;
            }
            long oldID = oldE.getId();
            AllocationEntry oldAE = oldTrace.getAllocationEntryFromToPointer(oldID, oldHeap.getTimestamp());
            if (oldAE == null) {
                continue;
            }
            JavaClass oldClazz = oldE.getClazz();

            for (int j = 0; j < newLength; j++) {
                JavaHeapObject newE = (JavaHeapObject) newElements[j];
                if (newE == null) {
                    continue;
                }
                if (objectsMatching.isMatchedNew(newE)) {
                    continue;
                }
                JavaClass newClazz = newE.getClazz();
                if (!classesMatching.isMatched(oldClazz, newClazz)) {
                    continue;
                }
                long newID = newE.getId();
                AllocationEntry newAE = newTrace.getAllocationEntryFromToPointer(newID, newHeap.getTimestamp());
                if (newAE == null) {
                    continue;
                }
                if (compareAllocationEntry(oldAE, newAE)) {
                    if (objectsMatching.addMatched(oldE, newE)) {
                        if (i != j) {
                            unmatchedIndexCount++;
                        } else {
                            matchedIndexCount++;
                        }
                        queue.add(oldE);
                        queue.add(newE);
                    }
                    break;
                } else {
                    compareAllocationEntry(oldAE, newAE);
                }
            }
        }
        int i, j = 0;
        for (i=0; i<oldLength; i++) {
            JavaHeapObject oldE = (JavaHeapObject) oldElements[i];
            if (oldE == null) {
                continue;
            }
            if (objectsMatching.isMatchedOld(oldE)) {
                continue;
            }
            JavaClass oldClazz = oldE.getClazz();
            for (; j < newLength; j++) {
                JavaHeapObject newE = (JavaHeapObject) newElements[j];
                if (newE == null) {
                    continue;
                }
                if (objectsMatching.isMatchedNew(newE)) {
                    continue;
                }
                JavaClass newClazz = newE.getClazz();
                if (!classesMatching.isMatched(oldClazz, newClazz)) {
                    continue;
                }
                if (objectsMatching.addMatched(oldE, newE)) {
                    queue.add(oldE);
                    queue.add(newE);
                }
                break;
            }
        }
    }

    protected void collectUntouchedObjects() {
        Enumeration<JavaHeapObject> oldObjects = oldHeap.getSnapshot().getThings();
        while (oldObjects.hasMoreElements()) {
            JavaHeapObject oldObject = oldObjects.nextElement();
            if (objectsMatching.isOld(oldObject) || objectsMatching.isMatchedOld(oldObject)) {
                continue;
            }
            AllocationEntry oldAE = oldTrace.getAllocationEntryFromToPointer(oldObject.getId(), oldHeap.getTimestamp());
            if (oldAE == null) {
                objectsMatching.addOld(oldObject);
                continue;
            }
            JavaHeapObject newObject = findMatchedNewObjectByAllocationEntry(oldObject, oldAE);
            if (newObject != null) {
                if (oldObject.getClass() != newObject.getClass()) {
                    objectsMatching.addOld(oldObject);
                    continue;
                }

                JavaClass oldClass = oldObject.getClazz();
                JavaClass newClass = newObject.getClazz();

                if (classesMatching.isMatched(oldClass, newClass)) {
                    if (!objectsMatching.addMatched(oldObject, newObject)) {
                        objectsMatching.addOld(oldObject);
                    } else {
                        untouchedMatchedCount ++;
                    }
                } else {
                    objectsMatching.addOld(oldObject);
                }
            } else {
                objectsMatching.addOld(oldObject);
            }
        }

        Enumeration<JavaHeapObject> newObjects = newHeap.getSnapshot().getThings();
        while (newObjects.hasMoreElements()) {
            JavaHeapObject newObject = newObjects.nextElement();
            if (objectsMatching.isNew(newObject) || objectsMatching.isMatchedNew(newObject)) {
                continue;
            }
            objectsMatching.addNew(newObject);
        }
    }

    protected void printAll() {
        super.printAll();
        System.out.println("There are " + unmatchedIndexCount + " objects matched by EI with unmatched indice.");
        System.out.println("There are " + matchedIndexCount + " objects matched by EI with matched indice.");
        System.out.println("There are " + untouchedMatchedCount + " unreachable objects matched by EI.");

        
        System.out.println("Old allocation sites: " + oldTrace.getAllocationCount());
        System.out.println("New allocation sites: " + newTrace.getAllocationCount());
    }
}

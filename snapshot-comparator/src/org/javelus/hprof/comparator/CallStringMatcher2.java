package org.javelus.hprof.comparator;

import java.util.LinkedList;

import org.javelus.minitracing.AllocationEntry;
import org.javelus.minitracing.ThreadTraces;

import com.sun.tools.hat.internal.model.JavaClass;
import com.sun.tools.hat.internal.model.JavaHeapObject;
import com.sun.tools.hat.internal.model.JavaObjectArray;
import com.sun.tools.hat.internal.model.JavaThing;

public class CallStringMatcher2 extends Matcher {

    public CallStringMatcher2(ObjectGraph oldHeap, ObjectGraph newHeap, ThreadTraces oldTrace, ThreadTraces newTrace, MethodMetaComparator comparator) {
        super(oldHeap, newHeap, oldTrace, newTrace, comparator);
    }

    protected void matchAndVisitJavaObjectArray(LinkedList<JavaHeapObject> queue,
            JavaObjectArray oldJOA, JavaObjectArray newJOA, JavaClass oldClass,
            JavaClass newClass) {
        final int oldLength = oldJOA.getLength();
        final int newLength = newJOA.getLength();

        JavaThing[] oldElements = oldJOA.getElements();
        JavaThing[] newElements = newJOA.getElements();
        for (int i=0; i<oldLength; i++) {
            JavaHeapObject oldE = (JavaHeapObject) oldElements[i];
            if (oldE == null) {
                continue;
            }
            long oldID = oldE.getId();
            AllocationEntry oldAE = oldTrace.getAllocationEntryFromToPointer(oldID, oldHeap.getTimestamp());
            if (oldAE == null) {
                if (i < newLength) {
                    JavaHeapObject newE = (JavaHeapObject) newElements[i];
                    if (newE != null 
                            && newTrace.getAllocationEntryFromToPointer(newE.getId(), newHeap.getTimestamp()) == null) {
                        // two objects have no id
                        compareAndAppendIntoQueue(queue, oldE, newE);
                    }
                }
                continue;
            }

            JavaHeapObject newE = findMatchedNewObjectByAllocationEntry(oldE, oldAE);
            if (newE == null) {
                if (i < newLength) {
                    newE = (JavaHeapObject) newElements[i];
                    if (newE == null) { // cannot match objects by access path 
                        continue;
                    }
                    AllocationEntry newAE = newTrace.getAllocationEntryFromToPointer(newE.getId(), newHeap.getTimestamp());
                    if (newAE == null) {
                        continue;
                    }
                    JavaHeapObject checkE = findMatchedOldObjectByAllocationEntry(newE, newAE);
                    if (checkE == null) {
                        // both oldE and newE has no matched objects via EI
                        // match them via access path
                        compareAndAppendIntoQueue(queue, oldE, newE);
                    }
                }
                continue;
            }
            if (compareAndAppendIntoQueue(queue, oldE, newE)) {
                continue;
            }
        }
    }

}

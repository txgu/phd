package org.javelus.hprof.comparator;

import java.util.HashSet;
import java.util.Set;

import com.sun.tools.hat.internal.model.AbstractJavaHeapObjectVisitor;
import com.sun.tools.hat.internal.model.JavaHeapObject;

class ReachableObjectsVisitor extends AbstractJavaHeapObjectVisitor {

    Set<JavaHeapObject> bag = new HashSet<JavaHeapObject>();

    @Override
    public void visit(JavaHeapObject t) {
        if (t != null && t.getSize() > 0 && !bag.contains(t)) {
            bag.add(t);
            t.visitReferencedObjects(this);
        }
    }
    
    public Set<JavaHeapObject> getReachableObjects() {
        return bag;
    }
}
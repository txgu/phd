package org.javelus.minitracing;

public class AllocationEntry {
    private final CallEntry parentCallEntry;
    private final ObjectReference object;
    private final int bci;
    private final int count;
    private final ThreadTrace threadTrace;

    public AllocationEntry(ThreadTrace threadTrace, CallEntry parentEntry, int bci, int count, ObjectReference object) {
        this.threadTrace = threadTrace;
        this.parentCallEntry = parentEntry;
        this.bci = bci;
        this.count = count;
        this.object = object;
        if (bci == -1 && !Main.GlobalLogger) {
            throw new RuntimeException("Oops");
        }
        if (bci == -2 && count != 1) {
            //throw new RuntimeException("Oops");
        }
    }

    public ThreadTrace getThreadTrace() {
        return threadTrace;
    }

    public int getCount() {
        return count;
    }

    public int getBci() {
        return bci;
    }

    public CallEntry getParentEntry () {
        return parentCallEntry;
    }

    public ObjectReference getObject() {
        return object;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        
        sb.append(object);
        sb.append('@');
        sb.append(bci);
        CallEntry entry = parentCallEntry;
        while (entry != null) {
            sb.append(':');
            sb.append(entry);
            entry = entry.getParent();
        }
        
        return sb.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + bci;
        result = prime * result + count;
        result = prime * result
                + ((parentCallEntry == null) ? 0 : parentCallEntry.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        AllocationEntry other = (AllocationEntry) obj;
        if (bci != other.bci)
            return false;
        if (count != other.count)
            return false;
        if (parentCallEntry == null) {
            if (other.parentCallEntry != null)
                return false;
        } else if (!parentCallEntry.equals(other.parentCallEntry))
            return false;
        return true;
    }

    
}

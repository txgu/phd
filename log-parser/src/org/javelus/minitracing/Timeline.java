package org.javelus.minitracing;

import java.util.Arrays;
import java.util.Set;

public class Timeline {
    
    public static Timeline buildTimeline(Set<Long> boxed) {
        long[] timestamps = new long[boxed.size()];
        int i = 0;
        for (long l:boxed) {
            timestamps[i++] = l;
        }
        Arrays.sort(timestamps);
        return new Timeline(timestamps);
    }
    
    private final long[] timestamps;

    public Timeline(long[] timestamps) {
        this.timestamps = timestamps;
    }

    public long getLastTimestamp() {
        return this.timestamps[timestamps.length - 1];
    }
    
    public boolean containedIn(Timeline all) {
        if (timestamps.length == 0) {
            return false;
        }
        long begin = timestamps[0];
        long end = timestamps[timestamps.length-1];
        return Arrays.binarySearch(all.timestamps, begin) != -1 && Arrays.binarySearch(all.timestamps, end) != -1 ;
    }


    public long before(long timestamp) {
        int index = Arrays.binarySearch(timestamps, timestamp);

        if (index == -1) {
            for (long time:timestamps) {
                System.out.println(time);
            }
            throw new RuntimeException("No timestamp " + timestamp);
        }
        
        if (index == 0) {
            return Long.MIN_VALUE;
        }

        return timestamps[index - 1];
    }
    
    public long after(long timestamp) {
        int index = Arrays.binarySearch(timestamps, timestamp);

        if (index == -1) {
            throw new RuntimeException("No timestamp " + timestamp);
        }
        
        if (index == timestamps.length - 1) {
            return Long.MIN_VALUE;
        }

        return timestamps[index + 1];
    }

    public long[] getTimestamps() {
        return timestamps;
    }
    
    
}

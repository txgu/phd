package org.javelus.minitracing;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ThreadTraces {

    static Pattern THREAD_LOG_PATTERN = Pattern.compile("mini_tracing_0x([0-9a-zA-Z]+).log");

    static Pattern METHOD_MAP = Pattern.compile("0x([0-9a-zA-Z]+) ([^ ]+) ([^ ]+) ([^ ]+)");

    public static ThreadTraces create(boolean globalLogger, File logDirectory) throws Exception {
        if (globalLogger) {
            return createForGlobalLogger(logDirectory);
        } else {
            return createForThreadLogger(logDirectory);
        }
    }

    public static ThreadTraces createForGlobalLogger(File logDirectory) throws Exception {
        List<ThreadTrace> threadTraces = null; // = new ArrayList<ThreadTrace>();
        Map<Long, ForwardPointers> forwardPointers = null;
        Map<Long, MethodMeta> methodMap = null;
        for (File f:logDirectory.listFiles()) {
            if (f.exists() && f.isFile()) {
                String name = f.getName();
                if (name.equals("forward_pointers.log")) {
                    forwardPointers = ForwardPointers.parse(f);
                    continue;
                }

                if (name.equals("method_map.log")) {
                    methodMap = MethodMap.parseMethodMap(f);
                    continue;
                }

                if (name.equals("mini_tracing_data.log")) {
                    threadTraces = ThreadTrace.parseForGlobalLogger(f);
                }
            }
        }

        return new ThreadTraces(methodMap, threadTraces, forwardPointers);
    }
    
    public static ThreadTraces createForThreadLogger(File logDirectory) throws Exception {
        List<ThreadTrace> threadTraces = new ArrayList<ThreadTrace>();
        Map<Long, ForwardPointers> forwardPointers = null;
        Map<Long, MethodMeta> methodMap = null;
        for (File f:logDirectory.listFiles()) {
            if (f.exists() && f.isFile()) {
                String name = f.getName();
                if (name.equals("forward_pointers.log")) {
                    forwardPointers = ForwardPointers.parse(f);
                    continue;
                }

                if (name.equals("method_map.log")) {
                    methodMap = MethodMap.parseMethodMap(f);
                    continue;
                }

                Matcher m = THREAD_LOG_PATTERN.matcher(name);
                if (m.matches()) {
                    long id = Long.parseLong(m.group(1), 16);
                    threadTraces.add(ThreadTrace.parseForThreadLogger(id, f));
                }
            }
        }

        return new ThreadTraces(methodMap, threadTraces, forwardPointers);
    }

    private final List<ThreadTrace> threadTraces;
    private final Map<Long, ForwardPointers> forwardPointers;
    private final Timeline timeline;
    private final Map<Long, MethodMeta> methodMap;

    public ThreadTraces(Map<Long, MethodMeta> methodMap, List<ThreadTrace> threadTraces, Map<Long, ForwardPointers> forwardPointers) {
        this.methodMap = methodMap;
        this.threadTraces = threadTraces;
        this.forwardPointers = forwardPointers;
        this.timeline = Timeline.buildTimeline(forwardPointers.keySet());
        if (!Main.GlobalLogger) {
            buildCallString();
            validateTimeline();
            validateAllocationSite();
        }
    }

    private void validateAllocationSite() {
        for (ThreadTrace tt:threadTraces) {
            CallString callString = tt.getCallString();
            for (ObjectReference or:callString.getAllocatedObjectReferences()) {
                AllocationEntry ae = getAllocationEntry(or.getId(), or.getTimestamp());
                if (ae == null) {
                    throw new RuntimeException("Validation failed!");
                }
                ae = getAllocationEntry(or);
                if (ae == null) {
                    throw new RuntimeException("Validation failed!");
                }
            }
        }
    }

    private void validateTimeline() {
        for (ThreadTrace tt:threadTraces) {
            if (!tt.getTimeline().containedIn(timeline)) {
                throw new RuntimeException("Cannot match timeline.");
            }
        }
    }

    public Timeline getTimeline() {
        return timeline;
    }

    protected void buildCallString() {
        for (ThreadTrace tt:threadTraces) {
            tt.buildCallString(this);
        }
    }

    /**
     * The id is logged before the GC happens at the timestamp.
     * Thus, we should first locate the ForwardPointers before the time stamp
     * and find the to until there is a map id.
     * @param timestamp
     * @param id
     * @return
     */
    public MethodMeta getMethodMeta(long timestamp, long id) {
        MethodMeta mm = this.methodMap.get(id);
        while (mm == null) {
            timestamp = timeline.before(timestamp);
            if (timestamp == Long.MIN_VALUE) {
                throw new RuntimeException("Oops.., method is not movable in JDK 8");
            }
            ForwardPointers fp = this.forwardPointers.get(timestamp);
            id = fp.getFrom(id);
            if (id == 0) { // id is the raw address, if the id is zero, then
                throw new RuntimeException("Cannot find method meta for id " + id);
            }
            mm = this.methodMap.get(id);
        }
        return mm;
    }

    public AllocationEntry getAllocationEntry(ObjectReference or) {
        for (ThreadTrace tt:threadTraces) {
            AllocationEntry entry = tt.getAllocationEntry(or);
            if (entry != null) {
                return entry;
            }
        }
        return null;
    }
    
    public ThreadTrace getThreadTraceByEETOP(long id) {
        for (ThreadTrace tt:threadTraces) {
            if (tt.getThreadID() == id) {
                return tt;
            }
        }
        return null;
    }
    
    public AllocationEntry getAllocationEntry(AllocationEntry ae) {
        for (ThreadTrace tt:threadTraces) {
            AllocationEntry entry = tt.getAllocationEntry(ae);
            if (entry != null) {
                return entry;
            }
        }
        return null;
    }

    public AllocationEntry getAllocationEntryFromToPointer(long toId, long timestamp) {
        ForwardPointers fp = this.forwardPointers.get(timestamp);
        if (fp == null) {
            throw new RuntimeException("New pointer should have a forward pointer.");
        }
        long fromId = fp.getFrom(toId);
        if (fromId == 0) {
            fromId = toId;
        }
        ObjectReference or = new ObjectReference(timestamp, fromId);
        AllocationEntry entry = getAllocationEntry(or);
        while (entry == null) {
            timestamp = timeline.before(timestamp);
            if (timestamp == Long.MIN_VALUE) {
                return null;
            }
            fp = this.forwardPointers.get(timestamp);
            toId = fromId;
            fromId = fp.getFrom(toId);
            if (fromId == 0) { // id is the raw address, if the id is zero, then the object is not move
                fromId = toId; // use the original id for next query
            }
            or = new ObjectReference(timestamp, fromId);
            entry = getAllocationEntry(or);
        }
        return entry;
    }
    
    /**
     * The id is logged before the GC happens at the timestamp.
     * Thus, we should first locate the ForwardPointers before the time stamp
     * @param timestamp
     * @param fromId
     * @return
     */
    private AllocationEntry getAllocationEntry(long fromId, long timestamp) {
        ObjectReference or = new ObjectReference(timestamp, fromId);
        AllocationEntry entry = getAllocationEntry(or);
        long toId;
        while (entry == null) {
            timestamp = timeline.before(timestamp);
            if (timestamp == Long.MIN_VALUE) {
                return null;
            }
            ForwardPointers fp = this.forwardPointers.get(timestamp);
            toId = fromId;
            fromId = fp.getFrom(toId);
            if (fromId == 0) { // id is the raw address, if the id is zero, then
                fromId = toId; // the object is not move;
            }
            or = new ObjectReference(timestamp, fromId);
            entry = getAllocationEntry(or);
        }
        return entry;
    }

    public long getForwardPointer(long id, long time) {
        ForwardPointers fp = this.forwardPointers.get(time);
        if (fp == null) {
            return 0L;
        }
        
        return fp.getTo(id);
    }
    
    public int getAllocationCount() {
        int count = 0;
        for (ThreadTrace tt:threadTraces) {
            count += tt.getCallString().getAllocatedObjectReferences().size();
        }
        return count;
    }

    public List<ThreadTrace> getThreadTraces() {
        return threadTraces;
    }

    public Map<Long, ForwardPointers> getForwardPointers() {
        return forwardPointers;
    }

}

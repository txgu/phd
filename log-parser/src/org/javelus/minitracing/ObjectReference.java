package org.javelus.minitracing;

public class ObjectReference {
    private final long timestamp;
    private final long id;

    public ObjectReference(long timestamp, long id) {
        this.timestamp = timestamp;
        this.id = id;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public long getId() {
        return id;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (id ^ (id >>> 32));
        result = prime * result + (int) (timestamp ^ (timestamp >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ObjectReference other = (ObjectReference) obj;
        if (id != other.id)
            return false;
        if (timestamp != other.timestamp)
            return false;
        return true;
    }

    public String toString() {
        return String.format("%d@%d", id, timestamp);
    }
}

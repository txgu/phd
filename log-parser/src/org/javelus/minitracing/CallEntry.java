package org.javelus.minitracing;

import java.util.HashMap;
import java.util.Map;



public class CallEntry {
    /**
     * Caller
     */
    private CallEntry parent;
    /**
     * index in the callee array in the caller
     */
    private int callerBci;
    private int count;
    /**
     * Depth, the first method is of depth 0
     */
    private int depth;
    /**
     * Method information
     */
    private MethodMeta method;
    
    private Map<Integer, Integer> bciToCount;
    
    CallEntry(CallEntry parent, int bci, int count, MethodMeta method) {
        this.parent = parent;
        this.callerBci = bci;
        this.count = count;
        this.method = method;
        if (parent != null) {
            depth = parent.depth + 1;
        }
        if (bci == -1 && parent != null && parent.getMethod() != CallString.ROOT) {
            throw new RuntimeException("Oops");
        }
        if (bci == -1 && count != 1) {
            throw new RuntimeException("Oops");
        }
    }

    public int getDepth() {
        return depth;
    }

    public CallEntry addCallee(MethodMeta method, int bci) {
        int count = getBciCount(bci);
        CallEntry entry = new CallEntry(this, bci, count, method);
        return entry;
    }

    private int getBciCount(int bci) {
        if (bciToCount == null) {
            bciToCount = new HashMap<Integer, Integer>();
        }
        Integer countBox = bciToCount.get(bci);
        int count;
        if (countBox == null) {
            count = 1;
        } else {
            count = countBox + 1;
        }
        bciToCount.put(bci, count);
        return count;
    }

    public int getCount() {
        return count;
    }

    public int getCallerBci() {
        return callerBci;
    }

    public MethodMeta getMethod() {
        return method;
    }

    public CallEntry getParent() {
        return parent;
    }

    private void toCallString(StringBuilder sb) {
        if (parent != null) {
            parent.toCallString(sb);
            sb.append("->");
        }
        sb.append(method);
    }

    public String toCallString() {
        StringBuilder sb = new StringBuilder();
        toCallString(sb);
        return sb.toString();
    }

    public String toString() {
        return "[" + callerBci + "]: " + method;
    }

    public AllocationEntry allocate(ThreadTrace threadTrace, ObjectReference object, int bci) {
        int count = getBciCount(bci);
        AllocationEntry entry = new AllocationEntry(threadTrace, this, bci, count, object);
        return entry;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + callerBci;
        result = prime * result + count;
        result = prime * result + depth;
        result = prime * result + ((method == null) ? 0 : method.hashCode());
        result = prime * result + ((parent == null) ? 0 : parent.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        CallEntry other = (CallEntry) obj;
        if (callerBci != other.callerBci)
            return false;
        if (count != other.count)
            return false;
        if (depth != other.depth)
            return false;
        if (method == null) {
            if (other.method != null)
                return false;
        } else if (!method.equals(other.method))
            return false;
        if (parent == null) {
            if (other.parent != null)
                return false;
        } else if (!parent.equals(other.parent))
            return false;
        return true;
    }
    
    
}

package org.javelus.minitracing;

public enum EventType {

    Nop(0),
    MethodEntry(1),
    MethodExit(2),
    ObjectAllocation(3),
    TimeStamp(4),
    LockAcquire(5),
    LockRelease(6);

    private int id;

    EventType(int id) {
        this.id = id;
    }

    public int getCppEnum() {
        return id;
    }
}

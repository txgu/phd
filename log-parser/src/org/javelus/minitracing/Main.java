package org.javelus.minitracing;

import java.io.File;

public class Main {

    static boolean GlobalLogger = false;

    static void printUsageAndExit() {
        System.out.println("Usage: [-g] <log-directory>");
        System.exit(1);
    }

    public static void main(String[] args) throws Exception {
        File logDirectory = null;

        for (int i = 0; i < args.length; i++) {
            String option = args[i];
            if (option.equals("-g")) {
                GlobalLogger = true;
            } else if (!option.startsWith("-")) {
                logDirectory = new File(option);
            } else {
                System.out.println("Unknown options: " + option);
                printUsageAndExit();
            }
        }

        if (logDirectory == null) {
            System.out.println("No log data directory");
            printUsageAndExit();
        } else if (!logDirectory.isDirectory()) {
            System.out.println("File " + logDirectory.getAbsolutePath() + " is not a directory.");
            printUsageAndExit();
        }

        ThreadTraces traces = ThreadTraces.create(GlobalLogger, logDirectory);
        
        for (ThreadTrace trace : traces.getThreadTraces()) {
            trace.printTrace();
        }
    }
}

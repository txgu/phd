package org.javelus.minitracing;

public class Event {
    
    public static Event createEvent(int type, long data, int bci) {
        if (type < 0) {
            throw new RuntimeException("Type is negative " + type);
        }
        EventType javaType = EventType.values()[type];
        return new Event(javaType, data, bci);
    }
    
    private final EventType type;
    
    private final long data;
    
    private final int bci;
    
    /*public */Event(EventType type, long data, int bci) {
        this.type = type;
        this.data = data;
        this.bci = bci;
    }
    
    public EventType getType() {
        return type;
    }
    
    public long getData() {
        return data;
    }
    
    public int getBci() {
        return bci;
    }
    
    public String toString() {
        if (bci < 0) {
            return String.format("0X%X %s", data, type);
        }
        return String.format("0X%X@%d %s", data, bci, type);
    }
}

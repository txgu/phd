package org.javelus.minitracing;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ForwardPointers {

    public static Map<Long, ForwardPointers> parse(File forwardPointersLog) throws IOException {
        Map<Long, Long> toToFrom = null;// = new HashMap<Long, Long>();
        Map<Long, Long> fromToTo = null;// = new HashMap<Long, Long>();
        DataInputStream input = null;
        Map<Long, ForwardPointers> results = new HashMap<Long, ForwardPointers>();
        byte readBuffer[] = new byte[8];
        try {
            input = new DataInputStream(new BufferedInputStream(new FileInputStream(forwardPointersLog)));
            while (true) {
                try {
                    long from = Utils.readLong(input, readBuffer);
                    long to = Utils.readLong(input, readBuffer);
                    if (from == 0) {
                        toToFrom = new HashMap<Long, Long>();
                        fromToTo = new HashMap<Long, Long>();
                        ForwardPointers fp = new ForwardPointers(to, toToFrom, fromToTo);
                        results.put(to, fp);
                    } else {
                        toToFrom.put(to, from);
                        fromToTo.put(from, to);
                    }
                } catch (EOFException e) {
                    break;
                }
            }
        } finally {
            if (input != null) {
                input.close();
            }
        }

        ForwardPointers fp = new ForwardPointers(Long.MAX_VALUE, Collections.<Long,Long>emptyMap(), Collections.<Long,Long>emptyMap());
        results.put(Long.MAX_VALUE, fp);

        return results;
    }

    private long id;

    private Map<Long, Long> toToFrom;
    private Map<Long, Long> fromToTo;

    public ForwardPointers(long id, Map<Long, Long> toToFrom, Map<Long, Long> fromToTo) {
        super();
        this.id = id;
        this.toToFrom = toToFrom;
        this.fromToTo = fromToTo;
    }

    public long getFrom(long to) {
        Long from = this.toToFrom.get(to);
        if (from == null) {
            return 0;
        }
        return from.longValue();
    }

    public long getTo(long from) {
        Long to = this.fromToTo.get(from);
        if (to == null) {
            return 0;
        }
        return to.longValue();
    }

    public long getId() {
        return id;
    }
}

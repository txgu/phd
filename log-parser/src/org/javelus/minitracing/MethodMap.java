package org.javelus.minitracing;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MethodMap {

    static Pattern METHOD_MAP = Pattern.compile("0x([0-9a-zA-Z]+) ([^ ]+) ([^ ]+) ([^ ]+)");
    
    static Map<Long, MethodMeta> parseMethodMap(File f) throws IOException {
        Map<Long, MethodMeta> methodMap = new HashMap<Long, MethodMeta>();
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(f));
            String line = null;
            while ((line = br.readLine()) != null) {
                Matcher m = METHOD_MAP.matcher(line);
                if (m.matches()) {
                    long id = Long.parseLong(m.group(1), 16);
                    MethodMeta mm = new MethodMeta(m.group(2), m.group(3), m.group(4));
                    MethodMeta mo = methodMap.put(id, mm); 
                    if (mo != null) {
                        if (!mm.equals(mo)) {
                            throw new RuntimeException("Duplicated method id: mm is " + mm + " and mo is " + mo);
                        }
                    }
                }
            }
        } finally {
            if (br != null) {
                br.close();
            }
        }
        return methodMap;
    }
}

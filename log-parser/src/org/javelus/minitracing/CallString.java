package org.javelus.minitracing;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class CallString {
    static MethodMeta ROOT = new MethodMeta("", "ROOT", "");

    private CallEntry root;
    private CallEntry current;

    private Map<ObjectReference, AllocationEntry> objectToAllocSite = new HashMap<ObjectReference, AllocationEntry>();

    private Map<AllocationEntry, AllocationEntry> allocSites = new HashMap<AllocationEntry, AllocationEntry>();

    private static final boolean debug = false;

    private static final boolean printUnmatched = false;

    public CallString() {
        current = root = new CallEntry(null, -1, 1 /*at leat call this*/, ROOT);
    }

    private void doMethodEntry(MethodMeta method, int bci) {
        current = current.addCallee(method, bci);
    }

    private void doMethodExit(MethodMeta method, int bci) {
        if (!method.equals(current.getMethod())) {
            if (printUnmatched) {
                System.out.println("Current is: " + current.toCallString());
                //System.out.println("Depth: " + current.getDepth());
                System.out.println("Unmatched method: get " + method + ", expected " + current.getMethod());
            }
            return;
        }
        current = current.getParent();
    }

    public void methodEntry(MethodMeta method, int bci) {
        if (debug) {
            System.err.println("<div><!-- Entry: " + method + "-->");
        }
        doMethodEntry(method, bci);
    }

    public void methodExit(MethodMeta method, int bci) {
        if (debug) {
            System.err.println("</div><!-- Exit: " + method + "-->");
        }
        doMethodExit(method, bci);
    }

    public CallEntry getRoot() {
        return root;
    }

    public void allocate(ThreadTrace threadTrace, ObjectReference object, int bci) {
        AllocationEntry ae = current.allocate(threadTrace, object, bci);
        if (objectToAllocSite.put(object, ae) != null ) {
            throw new RuntimeException("Duplicated object");
        }
        AllocationEntry existing = allocSites.put(ae, ae);
        if (existing != null) {
            throw new RuntimeException("Duplicated object");
        };
    }

    public String toString() {
        return "Current: " + (current == null ? "<null>" : current.toCallString());
    }

    public AllocationEntry getAllocationEntry(ObjectReference object) {
        return this.objectToAllocSite.get(object);
    }

    public AllocationEntry getAllocationEntry(AllocationEntry ae) {
        return this.allocSites.get(ae);
    }

    public Collection<ObjectReference> getAllocatedObjectReferences() {
        return this.objectToAllocSite.keySet();
    }

    public void printStatistics() {
        for (Entry<ObjectReference, AllocationEntry> entry: objectToAllocSite.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue().getParentEntry());
        }
    }
}

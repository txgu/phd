package org.javelus.minitracing;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class ThreadTrace {

    public static class Fragment implements Comparable<Fragment> {
        private long id; // the timestamp after a moving gc
        /**
         * events before the timestamp
         */
        private List<Event> events;

        public Fragment(long id, List<Event> events) {
            super();
            this.id = id;
            this.events = events;
        }

        public long getId() {
            return id;
        }

        public List<Event> getEvents() {
            return events;
        }

        @Override
        public int compareTo(Fragment o) {
            if (this.id > o.id) {
                return 1;
            }

            if (this.id < o.id) {
                return -1;
            }

            return 0;
        }
    }

    private static final boolean debug = false;


    static <K1,K2,V> void addToMapMap(Map<K1,Map<K2,V>> map, K1 key1, K2 key2, V value) {
        Map<K2, V> values = map.get(key1);
        if (values == null) {
            values = new HashMap<K2, V>();
            map.put(key1, values);
        }
        values.put(key2, value);
    }

    static <K,V> void addToMapList(Map<K, List<V>> map, K key, V value) {
        List<V> values = map.get(key);
        if (values == null) {
            values = new ArrayList<V>();
            map.put(key, values);
        }
        values.add(value);
    }

    public static List<ThreadTrace> parseForGlobalLogger(File traceLog) throws IOException {
        Map<Long, Map<Long, Fragment>> threadIdToFragments = new HashMap<Long, Map<Long, Fragment>>();
        Map<Long, List<Event>> threadIDToEvents = new HashMap<Long, List<Event>>();

        DataInputStream input = null;
        byte readBuffer[] = new byte[8];
        try {
            input = new DataInputStream(new BufferedInputStream(new FileInputStream(traceLog)));
            while (true) {
                try {
                    int type = Utils.readShort(input);
                    long data = Utils.readLong(input, readBuffer);
                    long id = Utils.readInt(input);

                    if (type == EventType.TimeStamp.getCppEnum()) {
                        for (Entry<Long, List<Event>> entry : threadIDToEvents.entrySet()) {
                            Long threadID = entry.getKey();
                            List<Event> events = entry.getValue();
                            Fragment tf = new Fragment(data, events);
                            addToMapMap(threadIdToFragments, threadID, data, tf);
                            threadIDToEvents.put(threadID, new ArrayList<Event>(1024));
                        }
                    } else {
                        Long threadID = id;
                        Event e = Event.createEvent(type, data, -1); // global logger has no bci
                        addToMapList(threadIDToEvents, threadID, e);
                    }
                } catch (EOFException e) {
                    break;
                }
            }
        } finally {
            if (input != null) {
                input.close();
            }
        }

        for (Entry<Long, List<Event>> entry : threadIDToEvents.entrySet()) {
            Long threadID = entry.getKey();
            List<Event> events = entry.getValue();
            Fragment tf = new Fragment(Long.MAX_VALUE, events);
            addToMapMap(threadIdToFragments, threadID, Long.MAX_VALUE, tf);
        }

        List<ThreadTrace> threadTraces = new ArrayList<ThreadTrace>(threadIdToFragments.size());
        for (Entry<Long, Map<Long, Fragment>> entry : threadIdToFragments.entrySet()) {
            if (entry.getValue() == null) {
                throw new NullPointerException();
            }
            threadTraces.add(new ThreadTrace(entry.getKey(), entry.getValue()));
        }

        return threadTraces;
    }

    public static ThreadTrace parseForThreadLogger(long id, File f) throws Exception {
        Map<Long, Fragment> fragments = parseThreadTraceFragments(f);
        return new ThreadTrace(id, fragments);
    }

    private static Map<Long, Fragment> parseThreadTraceFragments(File traceLog) throws IOException {
        List<Event> events = new ArrayList<Event>(1024); // this array is very large
        DataInputStream input = null;
        Map<Long, Fragment> results = new HashMap<Long, Fragment>();
        byte readBuffer[] = new byte[8];
        try {
            input = new DataInputStream(new BufferedInputStream(new FileInputStream(traceLog)));
            while (true) {
                try {
                    int type = Utils.readShort(input);
                    long data = Utils.readLong(input, readBuffer);
                    int bci = Utils.readInt(input);

                    if (type == EventType.TimeStamp.getCppEnum()) {
                        Fragment tf = new Fragment(data, events);
                        results.put(data, tf);
                        events = new ArrayList<Event>(1024);
                    } else {
                        Event e = Event.createEvent(type, data, bci);
                        events.add(e);
                    }
                } catch (EOFException e) {
                    break;
                }
            }
        } finally {
            if (input != null) {
                input.close();
            }
        }

        //if (!events.isEmpty()) {
        Fragment tf = new Fragment(Long.MAX_VALUE, events);
        results.put(Long.MAX_VALUE, tf);
        //}

        return results;
    }

    // tracing
    private long threadID;
    private Map<Long, Fragment> timestampToFragments;
    private Timeline timeline;
    private CallString callString;

    private Object javaObject;

    public ThreadTrace(long threadID, Map<Long, Fragment> timestampToFragments) {
        this.threadID = threadID;
        this.timestampToFragments = timestampToFragments;
        this.callString = new CallString();
        timeline = Timeline.buildTimeline(timestampToFragments.keySet());
    }

    public Timeline getTimeline() {
        return timeline;
    }

    void buildCallString(ThreadTraces trace) {
        List<Fragment> fragments = new ArrayList<Fragment>(timestampToFragments.values());
        Collections.sort(fragments);
        for (Fragment frag:fragments) {
            long timestamp = frag.getId();
            List<Event> events = frag.getEvents();
            for (Event e:events) {
                switch (e.getType()) {
                case MethodEntry:
                    callString.methodEntry(trace.getMethodMeta(timestamp, e.getData()), e.getBci());
                    break;
                case MethodExit:
                    callString.methodExit(trace.getMethodMeta(timestamp, e.getData()), e.getBci());
                    break;
                case ObjectAllocation:
                    callString.allocate(this, new ObjectReference(timestamp, e.getData()), e.getBci());
                    break;
                default:
                    ;
                }
            }
        }
        //System.out.println(callString);
        if (debug) callString.printStatistics();

        timestampToFragments.clear();
        timestampToFragments = null;
    }

    public long getThreadID() {
        return threadID;
    }

    public AllocationEntry getAllocationEntry(ObjectReference or) {
        return callString.getAllocationEntry(or);
    }

    public AllocationEntry getAllocationEntry(AllocationEntry ae) {
        return callString.getAllocationEntry(ae);
    }

    public CallString getCallString() {
        return this.callString;
    }

    public Object getJavaObject() {
        return javaObject;
    }

    public void setJavaObject(Object javaObject) {
        this.javaObject = javaObject;
    }

    public void printTrace() {
        System.out.format("Thread [%X]\n", threadID);
        if (this.timestampToFragments == null) {
            System.out.println("Not impelemnted yet");
            return;
        }
        Fragment[] fragments = new Fragment[this.timestampToFragments.size()];
        fragments = this.timestampToFragments.values().toArray(fragments);
        Arrays.sort(fragments);
        int count = 1;
        for (Fragment f:fragments) {
            for (Event e : f.events) {
                System.out.format("%7d %s\n", count++, e);
            }
        }
    }
}

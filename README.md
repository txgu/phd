## Build

Checkout the following projects.

* [PHD](https://bitbucket.org/txgu/phd): This project.
* [Developer Interface](https://bitbucket.org/javelus/developer-interface): Used to load a list of changed methods.
* [DPG](https://bitbucket.org/javelus/dpg): Used to generate a list of changed methods.
* [Javelus](https://bitbucket.org/javelus/javelus): Checkout the branch `mini-tracing`.
    * For Windows, you can find the detailed building guide of the HotSpot JVM at [here](http://moon.nju.edu.cn/dse/javelus/build_windows)
    * For Linux, check the simple bash script at `make/quick_build.sh`.


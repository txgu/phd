# README

For `jdk1.8.0_60` only.

We only hook methods in the following two kinds of locks.

* `java.util.concurrent.locks.ReentrantLock`
* `java.util.concurrent.locks.ReentrantReadWriteLock`
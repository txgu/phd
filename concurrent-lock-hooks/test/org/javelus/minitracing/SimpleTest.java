package org.javelus.minitracing;

import java.util.Random;
import java.util.concurrent.locks.ReentrantLock;

public class SimpleTest {

    public static class TestThread extends Thread {
        ReentrantLock lock;
        Random random = new Random();

        public TestThread(ReentrantLock lock) {
            this.lock = lock;
        }

        public void run() {
            for (int i = 0; i < 100; i++) {
                lock.lock();
                System.out.format("%s-%d\n", getName(), i);
                lock.unlock();
                randomSleep(100);
            }
        }

        void randomSleep(long t) {
            try {
                Thread.sleep(1 + Math.abs((random.nextLong() % t)));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) throws Exception {
        Thread[] threads = createThreads();

        for (Thread thread : threads) {
            thread.start();
        }

        for (Thread thread : threads) {
            thread.join();
        }
    }

    private static Thread[] createThreads() {
        ReentrantLock lock = new ReentrantLock();
        final int count = 5;
        Thread[] threads = new Thread[count];
        for (int i = 0; i < count; i++) {
            threads[i] = new TestThread(lock);
        }
        return threads;
    }
}

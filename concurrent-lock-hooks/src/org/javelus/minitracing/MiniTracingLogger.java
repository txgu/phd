package org.javelus.minitracing;

public class MiniTracingLogger {

    public static final int NOP = 0;
    public static final int METHOD_ENTRY = 1;
    public static final int METHOD_EXIT = 2;
    public static final int OBJECT_ALLOCATION = 3;
    public static final int TIMESTAMP = 4;
    public static final int LOCK_ACQUIRE = 5;
    public static final int LOCK_RELEASE = 6;

    public static void logEvent(int type, Object data) {
        switch (type) {
        case NOP:
        case METHOD_ENTRY:
        case METHOD_EXIT:
        case OBJECT_ALLOCATION:
        case TIMESTAMP:
            throw new IllegalStateException();
        case LOCK_ACQUIRE:
        case LOCK_RELEASE:
            logEvent0(type, data);
            break;
        }
    }

    private static native void logEvent0(int type, Object data);
}
